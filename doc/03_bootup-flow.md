## 上電後的執行流程

- 以 hello.asm 為例

  <img src="bootup-flow/boot-flow-0-code.png" width=300>

  > 注意，asm 檔需要經過編譯後才能被 bochs 使用，詳見 [bochs 的使用](01_setup-develop-env-arch.md)

  > nasm hello.asm -o hello.bin

  > dd if=hello.bin of=master.img bs=512 count=1 conv=notrunc

  > bochs -q

- 流程，
  - step，電腦上電後，CPU開始執行，CPU會尋找並執行位於ROM中的BIOS程序 (跳轉到 0xfe05b)

  - step，bios 接管控制權後，初始化CPU、記憶體、和其他硬體元件，並進行檢查(POST-Check)，以確保所有的硬體運行正常

  - step，bios 完成硬體的初始化和檢查後，BIOS將其自身的代碼從ROM複製到RAM，可以加速後續的啟動過程，以提高後續的啟動過程

    被複製的BIOS代碼會在RAM中佔據特定的地址空間，以免意外覆蓋這些重要的BIOS代碼
    - 以x86為例，通常位於 0xE0000 到 0xFFFFF 之間的地址空間，占用大約 64KB 的空間
    - 不同的主板設計可能略有不同，例如，嵌入式系統或特殊應用可能使用不同的地址範圍
    - 在本例中為 `0x7c00` 位址

  - step，尋找並執行 bootloader
    - 依照啟動順序設置的順序，依序查找啟動設備
    - 在啟動設備中，第一個 512 bytes 的空間稱為 master-boot-record (MBR)
    - bios會在當前啟動設備中，尋找MBR的空間內是否出現 0x55AA，
      - 第 511 byte 是否是 0x55
      - 第 512 byte 是否是 0xAA
      - 若是，代表第一個 512 bytes 空間的內容是 bootloader，
        bios 會將整個MBR載入記憶體中，並執行bootloader的機械碼

  - step，bootloader 負責將記憶體的操作模式從16bit的實模式，切換到保護模式
   
## 跳轉到 BIOS 的記憶體位址

- 跳轉到 `0xfe05b` 的記憶體位址 ( 將0xf000 寫入 cs，將 0xe05b 寫入 ip)，`0xfe05b` 是BIOS 的區域 
  
- 8086記憶體位址的計算，見 [8086 記憶體位址的計算](https://gitlab.com/anthony076/cpu_impl/-/blob/main/doc/stack%E3%80%81jump%E3%80%81function%20call%E3%80%81.interrupt.md)

<img src="bootup-flow/boot-flow-1-jump_to_0xfe05b.png" width=700>

## BIOS 的執行內容

- 檢測硬體是否正常
- 尋找可啟動設備，例如，可啟動的硬碟
- 將硬碟主引導磁區(硬碟的第一個磁區)的內容複製到 RAM `0x7c00` 的位置
- 檢查主引導磁區 的結尾是否是 0x55 0xAA
  - 若是，跳轉到 0x7c00 的位址，並執行代碼
  - 若否，停止執行

## 檢視 RAM `0x7c00` 的位置 (啟動程序的複製位置)

- 在 0x7c00 設置中斷點，設置後，`點擊 continue`，以觸發 0x7c00 的中斷點
  
  <img src="bootup-flow/boot-flow-2-set-breakpoint.png" width=700>

- `View > Physical MemDump`，檢視 RAM `0x7c00` 的區域
  
  <img src="bootup-flow/boot-flow-3-check-7c00.png" width=900>

- 開機磁區必須以 `0x55 0xAA` 結尾，才是有效的開磁區
  
  <img src="bootup-flow/boot-flow-4-must-end-with-0x55aa.png" width=500>

## 為什麼結束標記是 0x55aa

0x55aa = 0b0101_0101_1010_1010

0x55aa 轉換為二進制後可發現，0 和 1 是交錯出現的，
若主引導磁區出現壞軌，有可能會出現 0xFFFF 或 0x0000 的機率是最高，壞軌出現01交錯的形式是最低的，
透過將結束標記設置為 0 和 1 交錯排列的形式，可以盡早識別出主引導磁區是否壞軌，大幅度降低因主引導善區壞軌出現導引錯誤的可能

## 開機程序解析

- 開機程序 hello.asm

  ```asm {.line-numbers}
  <!-- 設定DS(資料段暫存器)的值 -->
  01 mov ax, 0xb800
  02 mov ds, ax

  <!-- 將 T 寫入顯示用的數據區，第0個位置 -->
  04 mov byte [0], 'T'
  05

  06 hlt:
  07   jmp hlt
  08
  <!-- 填入 498 個 0 -->
  09 times 510 - ($ - $$) db 0
  10
  <!-- 填入 0x55 和 0xAA -->
  11 db 0x55, 0xaa
  ```

- `Line1-2`，透過 ax 設置 ds 寄存器的值
  
  - DS 是段暫存器，類似 CS 和 SS 用來設定 RAM 的記憶體位址，DS 用來設定資料區 (data-segment) 在記憶體中的位置

  - 要設定段暫存器，需要透過 ax 暫存器

  - 參考，[堆棧基礎，記憶體管理](https://gitlab.com/anthony076/cpu_impl/-/blob/main/doc/stack%E3%80%81jump%E3%80%81function%20call%E3%80%81.interrupt.md)

- `Line4`，將 T 寫入顯示區，
  
  - 格式，`mov Byte [內存index] 寫入的資料`，未指明段寄存器時，預設使用 DS 指向的位置

    因此，`mov byte [0], 'T'`，會被 nasm 翻譯為 `mov byte ptr ds:0x0000, 0x54`，代表將資料 0x54 (0x54 = ASCII 的 'T') 寫入 DS 指向位置(0xb8000)，
    
  - 將資料寫入顯示區數據

    參考 [實模式下的內存分配區](#實模式下的內存分配區)，用於顯示的數據區為 `0xb8000 - 0xB8FFFF`，透過 DS 將RAM的位置定向到 `0xb8000` 的位址
  
    DS = 0xb800，實際的記憶體位址為 0xb800 * 16 + shift = 0xb800 * 16 + 0x0 = 0xb8000

    <img src="bootup-flow/boot-flow-5-write-display.png" width=500>

  - 若要更改文字樣式
    ```
    mov byte [0], 'T'
    mov byte [1], 11110000b
    ```

- `Line9`，times，連續初始化數值
  
  - 格式，times <初始化次數> <數據類型> <值>
    > 可用數據類型
    > - db，data-byte，數據類型為 byte
    > - dw，data-word，數據類型為 word
    > - dd，data-double-word，數據類型為 double-word
    > - dq，data-quad-word，數據類型為 quad-word
    > - dt，data-ten-byte，數據類型為 ten-byte

  - `$` 和 `$$`，代表在記憶體中的位置

    `$` 代表 line9 編譯後放在記憶體中的位置，即 $ = 0x7c00c

    `$$`，當前 section 起始位置，當前 setion 的起始為 0x7c000

    510 - ($ - $$) = 510 - ( 0x7c00c - 0x7c000 ) = 498，

    代表 從 0x7c00 的位置開始，已寫入 12 個指令，510 扣除已寫入的 12 的指令後，還需要填入 498 個 0

    <img src="bootup-flow/boot-flow-6-dollar-symbol.png" width=500>

  - `times 510 - ($ - $$) db 0`，代表 寫入 498 個，資料類型為 byte 的 0
    
    開機程序區域的長度為 512 Byte，扣除最後的 0x55 和 0xAA 需要手動寫入，

    因此總長度以 510 計算，最後，利用 ($ - $$) 計算需要填入 0 的個數

  
## 實模式下的內存分配區
- 為什麼啟動程序放在 0x7c00 的位置
    
  早期電腦為了兼容 IBM PC，`0x000 - 0x7DFF` 使用了與IBM PC 相同的內存配置，
  而 IBM PC 的啟動程序是放在 MBR 加載區，0x7c00 的位置，為了兼容 IBM PC，因此加載程序就統一放在 0x7c00 的位置

- IBM PC 5150 的內存分布 (32KB)
    
  | 起始位址 | 結束位置 | 大小    | 用途        |
  | -------- | -------- | ------- | ----------- |
  | 0x000    | 0x3FF    | 1KB     | 中斷向量表  |
  | 0x400    | 0x4FF    | 256B    | BIOS數據區  |
  | 0x500    | 0x7BFF   | 29.75KB | 可用區域    |
  | 0x7C00   | 0x7DFF   | 512B    | MBR加載區域 |
  | 0x7E00   | 0x7FFF   | 512KB   | MBR 棧空間  |

- x86 的內存分布
    
  | 起始位址 | 結束位置 | 大小     | 用途               |
  | -------- | -------- | -------- | ------------------ |
  | 0x000    | 0x3FF    | 1KB      | 中斷向量表         |
  | 0x400    | 0x4FF    | 256B     | BIOS數據區         |
  | 0x500    | 0x7BFF   | 29.75KB  | 可用區域           |
  | 0x7C00   | 0x7DFF   | 512B     | MBR加載區域        |
  | 0x7E00   | 0x9FBFF  | 607.6KB  | 可用區域           |
  | 0x9FC00  | 0x9FFFF  | 1KB      | 擴展BIOS數據區     |
  | 0xA0000  | 0xAFFFF  | 64KB     | 用於彩色顯示適配器 |
  | 0xB0000  | 0xB7FFF  | 32KB     | 用於黑白顯示適配器 |
  | 0xB8000  | 0xBFFFF  | 32KB     | 用於文字顯示適配器 |
  | 0xC0000  | 0xC7FFF  | 32KB     | 顯示適配器BIOS     |
  | 0xC8000  | 0xEFFFF  | 160KB    | 映射內存           |
  | 0xF0000  | 0xFFFEF  | 64KB-16B | 系統BIOS           |
  | 0xFFFF0  | 0xFFFFF  | 16B      | 系統BIOS入口地址   |

## 範例，顯示 hello world 的啟動程序

- 參考
  - [x86中斷向量表](https://zh.wikipedia.org/wiki/BIOS%E4%B8%AD%E6%96%B7%E5%91%BC%E5%8F%AB)

  - [螢幕控制](http://jendo.org/files/doc/ASM/asm9.pdf)
  
- 透過中斷清除螢幕
  ```
  mov ax, 3 
  int 0x10 
  ```
  觸發中斷前，需要將設定值寫入 ax，
  觸發中斷後，中斷程式會根據 ax 的值進行設定，
  ax = 3 代表 AH=00, AL=11
  ```
  AH 設定顯示模式，
  AH = 00，25*40，彩色，解析度360*400，16色
  AH = 01，25*40，彩色，解析度360*400，16色
  AH = 02，25*80，彩色，解析度720*400，16色
  AH = 03，25*80，彩色，解析度720*400，16色
  AH = 07，25*80，單色，解析度720*400

  AL 設定游標跟隨方式
  AL = 0，光標返回起始位置；
  AL = 1，光標跟隨移動；
  AL = 2，光標返回起始位置；
  AL = 3，光標跟隨串移動

  若不清除螢幕，AL 的第7位設置為1
  AL = 0b1xxxx xxxx
  ```
  
- 代碼 + 結果
  
  <img src="bootup-flow/boot-flow-7-helloworld.png" width=500>

## ref

- [OS development 101 - How to make a bootloader part 1](https://www.youtube.com/watch?v=E_n0VtGSZQY)