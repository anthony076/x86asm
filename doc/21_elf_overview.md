## PC 啟動流程

- 流程圖
  ```mermaid
  flowchart TD
  s1(CPU上電自我檢查) --> s2(進入 BIOS)
  s2 --> s3(將主引導磁區的內容複製到 0x7c00並執行)
  s3 --> s4(載入並進入 loader)
  s4 --> s5(內存檢測)
  s5 --> s6(進入保護模式)
  s6 --> s7(設置內存映射)
  s7 --> s8(加載OS內核)
  ```

- OS內核是一個 elf 文件

## ELF 文件概述

- ELF文件，Executable and Linking Format，可執行和可連結格式
  
- 是一種用於`可執行檔案`、`目的碼`、`共享庫`和`核心轉儲` 的標準檔案格式
  - 可執行程序，在 windows 下的 .exe 檔案

  - 可重定位文件，通常指靜態庫，例如，
    - 在 linux 的 .o 或 .a (.a 是 .o 的集合)
    - 在 windows 的 .lib

  - 共享目標文件，通常指動態庫，例如，
    - 在 linux 的 .so
    - 在 windows 的 .dll

- ELF 文件包含
  - `.text`，代碼段

  - 數據分為兩種， 
    - `.data`，已經初始化的數據段
      例如，int a = 123; 

    - `.bss`，未初始化的數據段，只知道占用空間大小，但還沒有實際數據
      - Block Started by Symbol，以符號開始的區塊
      - 一般作為緩存區大小的指示，
        
        例如，int a;

        會在加載應用時，根據 bss 大小，預留一段記憶體空間作為 buffer，
        待程式在運行期，執行初始化產生數據後，在將數據填入
        
- 範例，以 elf.asm 為例

  <img src="elf-overview/eo-1-read-elf.png" width=700 height=auto>

- 範例，手寫 bss + 檢視 obj/exe 的內容
  - 代碼

    <img src="elf-overview/eo-2-bss-example.png" width=700 height=auto>

  - 透過 gcc 編譯後，會出現 PIE 的提示

    <img src="elf-overview/eo-3-PIE-warning.png" width=600 height=auto>

    PIE，Position-Independency-Executable，與位置無關的可執行檔，與位置無關代碼可以加載到任何內存執行，
    
    不影響執行，但是會有安全性的疑慮

  - 透過 nasm 輸出 .o 檔

    `nasm -f elf32 elf.asm -o elf.o`

  - 連結成執行檔，並去除 gcc 在連結時自動添加的啟動函數

    `gcc -m32 elf.o -static -nostartfiles -o elf.out`

  - 檢查執行檔的內容

    `readelf -e elf.out` 或 `objdump -M intel -d elf.out`

    <img src="elf-overview/eo-4-execute-content-by-readelf.png" width=700 height=auto>

## 利用以下兩種方式來檢視 obj/執行檔 的內容

- 方法1，利用 readelf，輸出詳細
  
  - `readelf -e 執行檔名` ，輸出 Intel格式 的組合語言
  
- 方法2，利用 objdump，輸出精簡
  - `objdump -d 執行檔名` ，輸出 AT&T格式 的組合語言
  
  - `objdump -M intel -d 執行檔名` ，輸出 AT&T格式 的組合語言

## 去除 gcc 自動添加的啟動函數

- gcc 預設會將啟動程序添加到可執行文件中，並將使用者自定義的 main 函數重新包裝，由啟動程序去調用 main 函數，方便捕捉 main 函數的狀態

- 若不需要 gcc 添加啟動程序，可以透過以下命令進行連結
  - 代碼的入口函數已經是 _start()，默認尋找 _start()
    > gcc -m32 elf.o -o elf -nostartfiles

  - 代碼的入口函數不是 _start()，需要手動指定入口函數
    > gcc -m32 elf.o -o elf -nostartfiles -e main
