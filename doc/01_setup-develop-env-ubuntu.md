## 注意，不要使用本檔案，已棄用，僅供參考
因為 ubuntu 上的 bochs v2.6.11 有 bug，點擊 stack 時，bochs 會無反應，

需要手動安裝 bochs，可手動安裝 bochs v2.7.1 或 手動安裝 bochs v2.6.11 + patch，

ubuntu 上只發佈到 bochs v2.6.11，因此改用 archlinux

## Reference
- https://www.bilibili.com/video/BV1b44y1k7mT

- bochs 配置檔說明

  https://www.itread01.com/content/1547883742.html

## ENV
- virtualbox v6.1.32
- ubuntu 20.04

## Install
- 更新 apt
  $ sudo apt update
  $ sudo apt update
  
- $ sudo apt-get install nasm
- $ sudo apt-get install bochs
- $ sudo apt-get install bochs-x (for bochs gui on ubuntu)
- $ sudo apt-get install qemu
- $ sudo apt install qemu-utils (for qemu-img on ubuntu)
- VM WorkStation 16 Pro，Windows version

  https://www.vmware.com/go/getworkstation-win

## 建立 Bochs 開發環境
- Why Bochs

  Bochs 和 virtualbox、VMware 一樣都是虛擬機，Bochs 提供底層的界面和工具可以用來`調試組合語言`

- step1，建立並編譯 hello.asm

  - hello.asm
    
    ![image](setup/hello.asm.png)

  - 編譯命令
  
    `$ nasm hello.asm -o hello.bin`

- step2，建立 bochs 的設定檔 bochsrc

  `$ bochs -> select 4 -> input filename`

- step3，將 bochs 設置為GUI模式

  ![image](setup/configure-gui.png)

- step4，設置 bochs的啟動磁碟

  ![image](setup/configure-hd.png)

- step5，copy `hello.bin` into `master.img`，將 hello.bin 放入磁碟的映像檔中
  - `注意`，使用 dd 前，必須`使用 bximage 建立開機磁區`的內容，否則無法正確開機
  - 命令，dd，轉換並複製檔案

    > dd `if`=hello.bin `of`=master.img `bs`=512 `count`=1 `conv`=notrunc

  - 參數，if，input-file，輸入檔案
  - 參數，of，output-file，輸出檔案
  - 參數，bs，read and write up to BYTES bytes at a time，設定扇區大小
  - 參數，count，copy only N input blocks，寫入扇區的區塊數
  - 參數，conv，convert the file as per the comma separated symbol list，
  
    根據逗號分隔的符號列表轉換文件

    `注意`，conv 需要設置為 `notrunc`，do not  the output file，不會截斷輸出文件，否則輸出的 master.img 會只有 512 bytes

- step6，利用磁碟映像檔(master.img) 啟動 bochs
  - 輸入 `$ bochs -q` 啟動

    啟動畫面，需要按下 Continue 按鈕，才會開始載入
    ![image](setup/start-bochs-1.png)

    載入後
    ![image](setup/start-bochs-2.png)

- step7，利用 qemu-img 命令，將磁碟映像檔(master.img)轉換為 VMware 支持的格式

    `$ qemu-img convert -O vmdk master.img master.vmdk`
  
- step8，在 VMware 中進行測試

  - 建立新的VM

    ![image](setup/test-in-vmware-1.png)

  - 稍後安裝 os

    ![image](setup/test-in-vmware-2.png)

  - 選擇 linux

    ![image](setup/test-in-vmware-3.png)

  - 選擇 virtual disk

    ![image](setup/test-in-vmware-4.png)

  - 選擇 master.vmdk

    ![image](setup/test-in-vmware-5.png)

  - 選擇 keep existing format

    ![image](setup/test-in-vmware-6.png)

  - 啟動後，顯示 hello.asm 中的 T

    ![image](setup/test-in-vmware-7.png)

## 常用命令列表
- 將asm檔編譯為bin檔

  `nasm hello.asm -o hello.bin`

- 將 bin 檔案的內容複製到磁碟映像檔中

  `dd if=hello.bin of=master.img bs=512 count=1 conv=notrunc`

- 啟動 bochs

  `bochs -q`

- 利用 qemu-img 命令，將 master.img 轉換為 VMware 的虛擬磁碟 (*.vmdk)

  `$ qemu-img convert -O vmdk master.img master.vmdk`