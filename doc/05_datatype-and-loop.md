
## 可用數據類型

- db，data-byte，數據類型為 byte
  
  例如，dd 100，nasm 編譯得到 0x1a，占用  1-byte
  
- dw，data-word，數據類型為 word
  
  例如，dw 0x55，nasm 編譯得到 0x55 00，占用 2-byte
  例如，dw 0x55aa，nasm 編譯得到 0x55 0xaa，占用 2-byte

- dd，data-double-word，數據類型為 double-word
- dq，data-quad-word，數據類型為 quad-word
- dt，data-ten-byte，數據類型為 ten-byte

## loop 的範例
