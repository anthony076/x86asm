## 範例集

## 系統調用

- 常用的系統調用函數
  - 注意，Linux系統提供了大量的系統調用，可通過<unistd.h>、<sys/types.h>、<sys/stat.h>、... 等查看

  - exit（編號1）： 退出程序。
  - fork（編號2）： 創建一個新的進程，新進程是原進程的副本。
  - read（編號3）： 從文件描述符中讀取數據。
  - write（編號4）： 向文件描述符中寫入數據。
  - open（編號5）： 打開或創建一個文件。
  - close（編號6）： 關閉一個文件描述符。
  - waitpid（編號7）： 等待指定的子進程結束。
  - execve（編號11）： 加載並執行一個新的程序。
  - chdir（編號12）： 改變當前工作目錄。
  - time（編號13）： 獲取當前時間。
  - stat（編號18）： 獲取文件信息。
  - kill（編號37）： 發送信號給進程。
  - getpid（編號20）： 獲取當前進程的進程ID。
  - getuid（編號24）： 獲取當前進程的用戶ID。
  - socket（編號41）： 創建一個套接字。
  - connect（編號42）： 連接到一個套接字。

- 流程
  - step1，將系統調用編號放入 eax 寄存器。
  - step2，將相應的參數放入其他通用寄存器，如 ebx、ecx、edx 等。
  - step3，使用 int 0x80 指令觸發系統調用。
  - step4，系統調用完成後，結果通常存放在 eax 寄存器中。

- 範例
  ```assembly
    
    ;定義數據
    section .data
        hello db 'Hello, World!', 0

    ;定義入口位置
    section .text
        global _start

    _start:
        ; 輸出 'Hello, World!' 字符串到控制台
        mov eax, 4           ; 使用系統調用 4，表示 "write" 函數
        mov ebx, 1           ; 文件描述符 1 表示標準輸出 (stdout)
        mov ecx, hello       ; 字符串的地址
        mov edx, 13          ; 字符串的長度
        int 0x80             ; 進行系統調用

        ; 退出程序
        mov eax, 1           ; 使用系統調用 1，表示 "exit" 函數
        xor ebx, ebx         ; 返回碼為 0
        int 0x80             ; 進行系統調用
  ```