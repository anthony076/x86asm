## 硬碟讀寫

- 透過硬碟讀寫才能加載OS內核
  ```mermaid
  flowchart LR
  加電 --> 加載BIOS(ROM) --> 跳轉到主引導磁區0x7c00的位置並執行
  ```

  - 問題，主引導磁區僅有 512 Byte，`512 Byte = 開機程序 + 硬碟分區的訊息`，實際上，主引導磁區只有小於 512 B 的空間可供使用

  - 解決方式，透過讀取硬碟讀取新的程序，並加載到內存後，才能繼續往下執行

  <img src="rw-disk/rw-5-first-section.png" width=500 height=auto>

## 硬碟的基本概念

- 一個硬碟是由多個碟片組合而成
  - 每個碟片都有上下兩面，都可以用於儲存數據，每次碟片有兩個磁頭
  - 磁柱 (Cylinder)，多個磁片組成的同心圓的磁碟，不同高度的磁柱，代表不同的磁片
  - 磁頭 (Head)，用於指向不同磁區的起始位置
  - 磁軌 (Track)，磁頭移動的軌跡
  - 磁區 (Sector) / 扇區，將儲存空間進行分區，方便磁頭進行移動，為硬碟最小的儲存物理量，有 512 Byte 和 4K 兩種格式

- 磁碟容量計算
  - 磁軌容量 = 磁軌上的磁區數量 * 磁區空間大小 
  - 磁柱容量 = 磁頭數 * ( 磁軌上的磁區數量 * 磁區空間大小 )
  - 硬碟數量 = 磁柱數 * ( 磁頭數 * 磁軌上的磁區數量 * 磁區空間大小 )

- 邏輯磁區 (Block)
  - 用 sector 來儲存的效率差，因為一個 sector 只有 512 Bytes
  - 改用 block方式儲存，在 partition 時，可以指定 block 的大小
  - 磁頭可以一次讀取一個 block
  
## 硬碟協定的種類

 - IDE， Integraded Drive Electronics，集成驅動電子
  
   將硬碟和硬碟驅動控制器電路集合在一起
   
 - ATA，Advanced Technology Attachment，
   
   ATA 是標準化後的 IDE

 - SATA
 - NVME


## 讀寫硬碟的基礎，以 ATA 為例

- 方法1，(少用) CHS 模式，需要知道磁柱、磁頭、磁區的位置才能進行寫入
  
- 方法2，(常用) LBA 模式，Logical Block Address

- 8086 讀寫硬碟的端口

  | Primary 通道            | Secondary 通道 | in 操作      | out 操作     |
  | ----------------------- | -------------- | ------------ | ------------ |
  | Command Block Registers |
  | `0x1F0`                 | 0x170          | Data         | Data         |
  | `0x1F1`                 | 0x171          | Error        | Features     |
  | `0x1F2`                 | 0x172          | Sector count | Sector count |
  | `0x1F3`                 | 0x173          | LBA low      | LBA low      |
  | `0x1F4`                 | 0x174          | LBA mid      | LBA mid      |
  | `0x1F5`                 | 0x175          | LBA high     | LBA high     |
  | `0x1F6`                 | 0x176          | Device       | Device       |
  | `0x1F7`                 | 0x177          | Status       | Command      |

  Primary 對應 bochs 的 ata0，Secondary 對應 bochs 的 ata-1

  ![image](rw-disk/rw-1-disk-config-in-bochs.png)

- 硬碟上的LBA暫存器
  - LBA 暫存器，用於`描述一個扇區地址`，告訴硬碟要存取的磁區地址
  - LBA 暫存器分為兩種，LBA28 和 LBA48
  - LBA28，代表 LBA暫存器使用 28bit 的位址線來定址，最多可支援 12GB

- 使用到的 8086 端口
  - 參考，[ATA 控制](https://wiki.osdev.org/ATA_PIO_Mode#28_bit_PIO)
  - 0x1F0 / 16bit / 讀寫數據的端口
  
  - 0x1F2 / 設置要讀取或寫入的磁區數量
  
  - 0x1F3 ~ 0x1F5 / LBA暫存器的前24位 (bit0-bit23)
  - 0x1F6
    - bit0 - bit3 / LBA暫存器的第 24 位 ~ 第 27 位
    - bit4，bit4=0，代表存取主要硬碟(primary)，bit4=1，代表存取從屬硬碟(slave)
    - bit5，固定為1
    - bit6=0，代表 CHS 的操作模式， bit6=1，代表 LBA 的操作模式，
    - bit7，固定為1
  - 0x1F7，對 IN 操作和 對 OUT 操作來說，0x1F7 具有不同的意義
    - 對 `IN` 操作而言
      - bit0，表示 ERR，bit0=1，表示出現錯誤
      - bit3，表示 DRQ，bit3=1，表示數據準備完畢
      - bit7，表示 BUSY，bit7=1，表示硬碟忙碌中

    - 對 `OUT` 操作而言
      - 寫入 0xEC，表示識別應盤
      - 寫入 0x20，表示讀取硬碟
      - 寫入 0x30，表示寫入硬碟

## 讀取硬碟流程

- step1，將 dx 指向 `0x1f2` 端口，設定要讀取的磁區數量
  
- step2，將 dx 指向 `0x1f3、0x1f4、0x1f5、0x1f6` 的 `bit3-bit0` 端口，
  
  設定 LBA 暫存器的 內容，用於指定要讀取硬碟的位置

- step3，透過 `0x1f6` 端口，
  - bit4，設定存取主要硬碟或次要硬碟
  - bit6，設定操作模式為 LBA 或 CHS
   
- step4，將 dx 指向 `0x1f7` 端口，寫入 0x20，告知硬碟要執行讀取操作
  
- step5，透過 `0x1f7` 端口，讀取 bit3的值，判斷硬碟的數據是否準備完畢

- step6，透過 `es:di`，指令讀取的數據要存放的內存位置
  
- step7，將 dx 指向 `0x1f0` 的端口，準備接收數據
  
- step8，透過 `in` 指令從 dx 讀取數據並放入 ax，再從 ax 轉移到內存

  ```mermaid
  flowchart RL
  硬碟 --> IO接口 --> 0x1f0端口 --> dx暫存器 --> ax暫存器 --> 內存
  ```
- step8，判斷是否讀取完畢，若否，進入判斷迴圈
  
## 寫入硬碟流程

- step1，將 dx 指向 `0x1f2` 端口，設定要寫入的磁區數量
  
- step2，將 dx 指向 `0x1f3、0x1f4、0x1f5、0x1f6` 的 `bit3-bit0` 端口，
  
  設定 LBA 暫存器的 內容，用於指定要寫入硬碟的位置

- step3，透過 `0x1f6` 端口，
  - bit4，設定存取主要硬碟或次要硬碟
  - bit6，設定操作模式為 LBA 或 CHS
   
- step4，將 dx 指向 `0x1f7` 端口，寫入 0x30，告知硬碟要執行寫入操作

- step5，透過 `es:di`，指令讀取的數據要存放的內存位置
  
- step6，將 dx 指向 `0x1f0` 的端口，準備寫入數據
  
- step7，將資料從內存移到 ax，再透過 `out` 指令將 ax 的內容寫入 dx

  ```mermaid
  flowchart LR
  內存 --> ax暫存器 --> dx暫存器 --> 0x1f0端口 --> IO接口 --> 硬碟
  ```

- step8，透過 `0x1f7` 端口，讀取 bit7的值，判斷硬碟的數據是否寫入完成

## Bochs 的結果

- 注意，bocks 時會鎖定硬碟檔 (master.img)，將 bochs 關閉後再查看

- 讀取結果

  <img src="doc/../rw-disk/rw-2-read-result.png" width=500, height=auto>

- 寫入結果，0x400 是第 2 個磁區 

  <img src="doc/../rw-disk/rw-3-write-result.png" width=500, height=auto>
