## 從硬碟讀取內核加載器

- PC 啟動流程

  ```mermaid
  flowchart TD
  b1(CPU加電自檢) --> b2(進入 BIOS) --> b3(bios 將主引導磁區複製到 0x7c00 並執行) 
  b3 --> b4(讀取內核加載器 kernal-loader 並執行) --> b5(由 kernal-loader 加載操作系統)
  ```

- 為什麼不能在主引導磁區(Main-Boot-Record, MBR)加載作業系統，因為空間不足，512 Byte 主引導磁區由以下組成
  - 446 Byte，bootloader
  - 64 Byte，硬盤分區表
  - 2 Byte，結束標記，0x55AA

  <img src="rw-disk/rw-5-first-section.png" width=500 height=auto>

- 因為主引導磁區不足，因為會將`內核加載器`的內容會`儲存在硬碟中`，
  
  在 bootloader 的引導程序中，透過`硬碟讀取`的方式，將內核加載器讀入內存並執行

- 從硬碟讀取內核加載器
  - step1，nasm 會將 loader.asm 編譯為 loader.bin
  - step2，透過 dd 命令，將 loader.bin 的內容複製到硬碟檔，master.img 的檔案中

    > dd if=`loader.bin` of=master.img bs=512 `count=4` `seek=2` conv=notrunc

    - count=4，會處理 loader.bin 的前4個磁區
    - seek=2，會將前4個磁區的內容，複製到 master.img 的第2個磁區中
    - 以上命令會將，loader.bin 的前四個磁區的內容，複製到 master.img 的第二個磁區，且長度為4個磁區

  - step3，在 boot.asm 中，用於配置讀取硬碟位置的暫存器
    - edi暫存器，用於設定讀取的硬碟數據，在內存的存放位置
      
      `edi = 0x1000 (在 loader.asm 中，指定了內存的位置為 0x1000)`

    - ecx暫存器，用於設置硬碟起始的磁區位置
      
      `ecx = seek = 2`

    - bl暫存器，用於設定讀取硬碟磁區的數量

      `bl = count = 4`

  - step4，boot.asm 的完整流程
    - 4-1，配置硬碟讀取的位置後
    - 4-2，從硬碟讀取 laod.bin 的內容後，將內容複製到 0x1000 的內存位置
    - 4-3，透過 jmp 0:0x1000 的指令，跳轉到 0x1000 的位置後繼續執行 load.bin 的內容
