## 輸入輸出基本概念

- CPU 控制外設有兩種方式
  - 以下兩種方法都是`透過外設內存來控制外設`，差別在於外設內存和CPU內存的架構不同
    
  - 方法1，透過映射內存，稱為 memory-mapped-io (MMIO)    
    - 在硬體上，需要一個地址解碼的晶片，用於將外設內存串接到CPU內存中 (CPU內存和外設內存同時有效)
      
      <img src="input-output/io-1-MMIO.png" width=500 height=auto>

    - CPU 訪問某個內存地址時，有可能是物理內存，也可能是某個IO設備的內存

    - 沒有 in / out 指令，只接透過`直接讀寫內存`與外設通訊，將外設的內存映射到CPU使用的內存中，內存和IO設備共享同一個 addres-space
    

  - 方法2，透過端口，稱為 port-mapped-io (PMIO) 或 isolated-io
    - 此架構下，CPU內存和外設內存的使用是分離的，有各自的地址空間
    
    - 此方法中，因為CPU內存和外設內存的`地址重疊`，因此使用上`外設內存稱為 port`，用來與CPU用的內存加以區別
        
    - 在CPU的物理接口上，CPU 需要`增加一條 io 專用的控制線`，用於在CPU內存和外設內存之間切換 (同時間只有一其中一個內存是有效的)
      
      <img src="input-output/io-2-PMIO.png" width=500 height=auto>

    - 具有 in / out 指令，以`操作寄存器`的形式與外設內存進行讀寫

      in/out 指令會在`讀取/寫入前`，透過 `IO/M pin`，將CPU內存切換為外設內存

      例如，in 0x1234, 0xffff，將數據 0x1234 寫入外設內存 0xffff 中

    - 外設內存實際上是將位址線接到一個解碼器，將位址解碼後，將訊號送到IO的控制電路中

      對 PMIO 來說，`位址相當於是 Interface 介面的控制訊號`，不像CPU內存是儲存裝置
      
      <img src="input-output/io-3-io-port.png" width=500 height=auto>
    
- 以 PMIO 為例，CPU <--> port 端口 <--> Interface 接口 <--> 外設
  - 差異
    | 組成              | 功能                                                                         |
    | ----------------- | ---------------------------------------------------------------------------- |
    | 端口 (Port)       | 由一個或多個寄存器所組成，作為 CPU 和 IO 之間的緩衝                          |
    | 接口 (IO 或 GPIO) | 外設相串聯的物理界面，用於緩存數據/狀態/控制等訊息、隔離或匹配電氣、訊息轉換 |

  - CPU 透過端口發送控制訊號或數據給 GPIO

  - GPIO 的接口電路，用地址線進行相關電路的控制
  
    <img src="input-output/io-4-interface-circuit.png" width=500 height=auto>

  - 在 GPIO 處進行數據緩衝/電氣隔離/訊號轉換後，傳遞給外設 
  
  - 外設收到來自GPIO的訊號後，利用自身的控制器，對訊號進行解譯後進一步控制設備

  - 外設`也是一個微控制器`，透過指定存取`外設記憶體的位址`和提供相關數據，就可以實現`對外設的控制`，
  
    因此，CPU 將要存取外設記憶體的位址和數據寫入端口，端口會透過GPIO轉發給外設，外設根據接收的位址和數據進一步對設備進行控制

- CPU 的控制端口和VGA的控制內存
  - 在CPU側，Intel 可用端口，0x0000 ~ 0xffff
    - CRT 地址端口位址 @ 0x3D4
    - CRT 數據端口位置 @ 0x3D5
    - CPU 需要將 CRT 的控制訊號寫在 0x3D4 和 0x3D5 的端口(外設內存)中
  
  - 在外設側，CRT 的控制晶片的控制方式
    - 參考，螢幕的控制標準

        [Hardware Level VGA and SVGA Video Programming Information Page](http://www.osdever.net/FreeVGA/vga/crtcreg.htm)

    - 0Eh，控制VGA游標位置的高8位暫存器 @ 外設
    - 0Fh，控制VGA游標位置的低8位暫存器 @ 外設
  
  - 例如，要將 Cursor 的位置寫入外設

    ![image](input-output/io-5-control-diagram.png)

## in 指令，CPU從外設接收數據
- 格式  
    | 寫法      |                                               |
    | --------- | --------------------------------------------- |
    | in al, dx | dx 為端口位址，將端口位址內的值，轉存在 al 中 |
    | in ax, dx | dx 為端口位址，將端口位址內的值，轉存在 ax 中 |

## out 指令，CPU發送數據給外設
- 格式  
    | 寫法       |                                               |
    | ---------- | --------------------------------------------- |
    | out dx, al | dx 為端口號，al 為寫入的值，寫入 8bit 的數據  |
    | out dx, ax | dx 為端口號，al 為寫入的值，寫入 16bit 的數據 |