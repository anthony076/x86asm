## 堆棧和函數調用

- `程序指令`的在記憶體中的位址，是由 `CS:IP` 決定的，CS:IP 決定下一個指令的位址

    例如，jmp 0x3:0x456，代表下一個指令在 0x3456 的位址
  
- `函數調用`的前後的位址，是由 `SS:SP` 決定的，SS:IP 是 stack 版的 CS:IP
  
  由於函數的調用需要區分順序，需要 FILO (LIFO) 的資料結構來保留函數調用的地址，

  因此，使用 stack區作為函數調用時使用的特殊記憶體區域

- SS 用於指定 stack 的段地址，SP 是 stack 區的偏移量，指向棧頂
  
  - `入棧時，SP-1`，`出棧時，SP+1`
  - stack 中的資料，是從記憶體的`尾部開始放置`
    
    ![image](stack-funcion-call/stack-1.png)

    - 初始 sp = stack 的最大值
    - sp=0，代表 stack 已滿

    因為 sp 指向棧頂，每次放入資料 sp -1，
    因此，利用 sp 可以設置 stack 區域的大小，因為初始的 sp 指向最大值

  - 透過 push指令 / pop 指令來存取 stack

## stack 的初始化
- 在使用 stack 前，必須先進行 stack 的初始化，設定 `stack 的段地址 `和 `stack 的大小`
- 例如，
  ```asm
    ; 初始化 stack 的位置和大小
    mov ax, 0
    mov ss, ax      ; 設置 stack 的段地址
    mov sp, 0x7c00  ; 設置棧頂的位置 ( 設置棧的大小 )
  ```
## PUSH 指令 / POP 指令，
- 利用 PUSH 執行入棧指令，入棧後，`SP = SP - 1`
  
  若有指定資料長度，會執行連續的 PUSH，`SP = SP - 資料長度`
  
  利用 POP 執行出棧指令，出棧後，`SP = SP + 1`

- 範例
  
  ![image](stack-funcion-call/stack-2-push-pop.png)

## call 指令，函數調用指令
- 遠程函數調用的兩種寫法
  - 方法1，(常用) 直接指定位址，此方法`可省略 far 關鍵字`，例如，
  
    `jmp 0x0:0x7c00`
  
  - 方法2，(不常用/中斷向量表用) 間接定址，透過內存指定位址，例如，
    ```asm
    jmp far [start]
    start: 
        dw 0x7c00, 0x0  ; 注意，先填入偏移地址，再寫段地址
    ```

- call 指令實際執行的內容
  - step1，自動將 call 指令的`下一行指令的位址`，推入 `stack` 中，用於函數返回地址
  - step2，將函數的地址寫入` CS:IP` 中，用於跳轉到函數位址
  - step3，跳轉到 CS:IP 指定的位置
  
    ![image](stack-funcion-call/stack-3-call-instruction.png)

## ret 指令，函數返回指令
- ret 指令實際執行的內容
  - step1，自動從 stack 取出函數調用前的位址
  - step2，將 pop 得到的位址寫入 CS:IP
  - step3，跳轉到 CS:IP 指定的位置

    ![image](stack-funcion-call/stack-4-ret-instruction.png)



