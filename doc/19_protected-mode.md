## 為什麼需要保護模式

- 保護模式和實模式的差異
  - 實模式(real-mode)，代表可以存取內存的任意位置，`不會有任何限制`，容易產生資料洩漏或病毒感染
  - 保護模式(protected-mode)，代表可以存取內存是`有限制的`，無法隨意訪問，避免實模式下的安全問題
  - [實模式基礎](./06_address-mode.md#實模式下real-mode記憶體地址的計算方式)

- 歷史
  - 1982年，intel 在 80286 引入了 16bit 的保護模式
  - 1985年，intel 在 80386 引入了 32bit 的保護模式 (兼容機)
  - 1989年，intel 在 80486 引入了 內存分頁的機制

- 保護資訊的範圍
  - 暫存器，某些控制用的暫存器，只有系統內核可以使用
  - 內存，使用`描述符`來保護內存，描述符紀錄了`內存的使用權限`
  - 外設，例如硬碟，由 CPU控制 in/out 指令的使用權限

    預設應用程式不能執行 in/out 指令，但可以向內核申請

- 保護模式是利用`記憶體管理(Memory Management)`來實現
  
- 記憶體管理利用了`描述符`來限制進程可存取的區域
  
  `描述符`代表數值一次描述`多種屬性`，而不是只描述一種屬性
 
## 描述符的來由

- 在實模式下，要存取一個記憶體位址，需要 Segment + Offset
  - Segment 實際上就是一個基礎地址 (BaseAddr)
  
  - 要將記憶體進行分段，`需要加入段的最大長度的限制(Limit)`，

    一個段的Offset，應該要小於 Limit，否則就會超過段的範圍
   
  - Offset 和 Limit 都是利用 16bit 的暫存器來指定，16bit 最大的定址空間為 2<sup>16</sup> = 2<sup>6</sup> * 2<sup>10</sup> = 64KB，
    因此，Offset 和 Limit 的最大值也都是 64KB，
    
    實模式下，Offset 最大的偏移量為 64KB，段的大小最大也是 64KB，Offset 並不會超過段的大小，因此就不需要特別指明 Limit 的大小

  - 16bit的段暫存器透過 `段暫存器 << 4 + Offset` 的方式，得到20bit的基礎地址(BaseAddr)後，就可以利用20根地址線來循址 
  
- 對於一個 32bit 的CPU，BaseAddr、Offset、Limit 都可以操作在 32bit 的實模式，但為了兼容 16bit 的暫存器，32bit的保護模式限制了，
    1. 段的 BaseAddr 只能是16的倍數，
    2. 段的 Limit 只能爲固定值64 KB，

- 保護模式下，除了 BaseAddr、Limit、Offset 外，為了實現記憶體的保護，需要加上訪問的權限 (Access) 等相關控制，共占用 64bit (8 byte)
  
  但為了兼容，且暫存器的長度只有16bit，無法直接將 64 bit的資訊放到16bit的暫存器中，
  因此，只能把 64bit 的資訊寫成固定格式的數據結構 (GDT) 放到內存中，再利用段寄存器中的值作爲索引來間接引用

- 實際上，每個段寄存器的實際長度都大於16bit，但為了兼容，段寄存器的可見部分只有16-bit的

## 描述符基礎

- 描述符區分為 `本地描述符 (Local Descriptor Table，LDT)` 和 `全局描述符 (Global Descriptor Table，GDT)`
  
  - 全局描述符: `用於系統`，是第一級的控制，所有程序共享一個 GDT
  - 本地描述符: `用於進程或應用程序`，是第二級的控制，描述進程任務的代碼段、數據段、堆棧段 的存取範圍
  
  - 保護模式一定要有 GDT，LDT 可以沒有

- 實模式下和保護模式下的`記憶體位址長度`是不一樣的
  - 保護模式下的記憶體位址是 `32bit` 的`邏輯地址`
    - 保護模式下的邏輯地址會經過以下其中一種狀況，將邏輯地址轉換為實體物理地址
      - 狀況1，GDT
      - 狀況2，GDT + LDT
      - 狀況3，GDT + LDT + 分頁機制
  
    - 保護模式下，段暫存器變成了段選擇子(segment-selector)，保存了GDT/LDT中描述符的索引值，從段描述符得到段的基址後，加上邏輯地址中的的偏移，就可以得到實體地址

    - 因為需要提供更多的訊息才能進行轉換，因此保護模式下的地址比實模式的地址長 (邏輯地址 32bit，實體地址 16bit)

    - 保護模式的邏輯地址經轉換後，才會得到 16bit的實體物理地址

  - 實模式下的記憶體位址是 `16bit` 的`實體物理地址`

- 透過描述符存取的是`線性地址空間`，不是`物理地址空間`
  - 在程式語言中存取的記憶體地址為邏輯地址
  - `線性地址`（Linear Address） 是邏輯地址到實體地址變換之間的`中間層`
    ```mermaid
    flowchart LR
    邏輯地址 --> 線性地址 --> 實體物理地址
    ```
  - 在`實模式`下，沒有內存的保護，此時 `邏輯地址 == 物理實體地址`
  - 在`保護模式`下，需要內存的保護，邏輯地址 + GPT 後，得到線性地址，
    - 若有啟用分頁機制，線性地址`經過轉換`後，才能到物理實體地址
    - 若未啟用分頁機制，線性地址 == 物理實體地址
  
  - 圖解邏輯地址 / 線性地址 / 物理地址 三者的差異
  
    <img src="protected-mode/pm-9-linear-addr-space.png" width=600 height=auto> 
  
- GDTR / GDT / Selector / LDTR / LDT 關係圖
  
  - GDT 是由多種描述符組合而成的，描述符可以是
    - 系統段描述符
    - 共享代碼段的描述符
    - 共享數據段的描述符
    - 共享棧段的描述符
    - LDT-Descriptor，用於描述 `LDT 的位置`
  
  - LDT 是由`多個 Segment-Descriptor 所組成`，Segment-Descriptor 用於描述進程中 `代碼段、數據段、堆棧段等位置`
  - Segment-Selector 段選擇子，就是實模式下的段暫存器，用於選擇 GDT 或 LDT 中有效的段描述符，選中後，透過指令將選中的描述符加載到暫存器中
  
  - <font color="blue">以 LDT 為例</font>
    
    <img src="protected-mode/pm-2-LDT-in-GDT.png" width=500 height=auto> 

    關係圖 ([from](https://xem.github.io/minix86/manual/intel-x86-and-64-manual-vol3/o_fe12b1e2a880e0ce-103.html))
  
    <img src="protected-mode/pm-6-GDT-LDT-relation.png" width=600 height=auto> 

    <br>

    | 名稱                           | 功能                                                                                      |
    | ------------------------------ | ----------------------------------------------------------------------------------------- |
    | GDT 全局描述符                 | 可放在任意位置，由多個描述符 組成                                                         |
    | GDTR 暫存器                    | 指向 GDT 的起始位址                                                                       |
    | Segment Selector <br> 段選擇子 | 可用於 GDT 或 LDT，用於選擇有效的段描述符                                                 |
    | LDTR 暫存器                    | 根據 Selector 的選擇，保存有效 LDT-Descriptor 的內容，<br> LDT-Descriptor 紀錄 LDT 的基址 |
    | LDT 本地描述符                 | 由多個 Segment-Descriptor 組成，描述 代碼段、數據段、堆棧段等位置、                       |
  
- GDT + LDT 如何限制應用程式可存取的記憶體
  - 流程

    ```mermaid
    flowchart TD 
    b1(透過 GDTR 取得 GDT 的基址) --> b2(透過 Selector 選中GDT中有效的 LDT-Descriptor) 
    b2 --> b3(將有效的 LDT-Descriptor保存在 LDTR暫存器中)
    b3 --> b4(透過 LDTR 取得 LDT 位址)
    b4 --> b5(LDT中的Segment-Descriptor描述每個段的有效位置)
    ```
  
  - 由流程圖可知，透過 Selector，GDT中只有一個 LDT-Descriptor 是有效的，

    透過 Selector，LDT中只有一個 Segment-Descriptor 是有效的，

    Selector 使得進程任務只能在描述符宣告的記憶體範圍內移動，超出範圍就會觸發異常中斷

## GDTR 暫存器，描述 GDT 的位置和大小

- 由實模式進入到保護模式，需要加載 GDT
  
- 上電後，GDTR中的基址會被預設為 0，長度值被預設為 0xFFFF，
  
  在保護模式初始化的過程中，需要給 GDTR 賦予新值

- GDTR暫存器的內容，是一個 `GDT的描述符(GDT Descriptor)`，用於描述 `GDT的大小` 和 `GDT的基址` 兩個屬性
  
  <img src="protected-mode/pm-5-GDTR.png" width=500 height=auto>
  
   - bit0-15，描述 GDT 的大小，固定為 2 Byes (16bit) 長度
    
   - bit16-47 或 bit14-79，用 32bit 或 64bit 長度的數值，來描述GDT的位置，是`線性地址`，不是物理實體地址

- GDTR 相關指令
  - ` LGDT 指令`，Load GDT，用於將 GDT 的入口地址加載到GDTR暫存器中

    語法，`LGDT [ gdt_ptr ]`，gdt_ptr 指向的位址，必須符合`GDT描述符`的數據結構，描述GDT的大小和GDT的基址，LGDT 會將 `GDT描述符`的內容放入 `GDTR` 暫存器中

  - `SGDT 指令` (Store GDT)，將 GDTR暫存器的內容，保存到指定的目的操作數中，用於 GDT 的切換

    語法，`SGDT [ gdt_ptr ]`

  - 注意，在將 GDTR 存放到記憶體中的時候（使用 SGDT 指令），會在記憶體中存放一個 48-bit pseudo-descriptor。因為這個 pseudo-descriptor 是由一個 16 bit 的邊界值和 32 bit 的基底位址所組成的，因此，在存放 GDTR 時，要注意對齊的問題。
    
    要避免在某些狀況下（CPL = 3 且 EFLAGS 中的 AC = 1 時）發生 alignment check fault，最好是把 pseudo-descriptor 放在「奇數位址」，即除以 4 會餘 2 的位址。這樣，16 bit 的邊界會對齊在 2 的倍數，而 32 bit 的基底位址也會對齊在 4 的倍數，就不會發生 alignment check fault 了。
    
## Segment-Selector，段選擇子，選擇有效的段描述符

- 用於選擇GDT或LDT中有效的段描述符

- 實際上，段選擇子就是實模式下的段暫存器 (cs/ds/es/ss/fs/gs)，
  
  不同的是，在保護模式下，`段暫存器不指向最終位置的段`，而是指向 GDT 或 LDT 中，有效描述符的 index，

  因此，保護模式下，將段暫存器 `改名為段選擇子`，但實質上和段暫存器是同一個暫存器，但段選擇子有固定的格式

- 在保護模式下，段基址已經存放在段描述符中了，因此，段寄存器的作用就改為了存儲GDT或LDT的索引
  
- Segment-Selector，段選擇子的內容
  
  <img src="protected-mode/pm-4-selector.png" width=300 height=auto>
  
  <BR>

  | 位置     | 長度  | 符號  | 意義                                                               |
  | -------- | ----- | ----- | ------------------------------------------------------------------ |
  | bit 0-1  | 2bit  | RPL   | Request-Privilege-Level，Selector 的特權等級 <br> 表示請求者的權限 |
  | bit 2    | 1bit  | TI    | Table-Indicator，TI=0=GDT，TI=1=LDT                                |
  | bit 3-15 | 13bit | Index | Descriptor 的索引值                                                |

- Selector 的使用範例
  - 保護模式下要使用邏輯地址，語法為 `[Selector] : [Offset]`

  - 例如，21h:12345678h，其中，
    - 21h 為 Selector = 0b0000_0000_0010_0001 = 100_0_01
  
      RPL=1，TI=0，選中GDT，index=0b100=4，選擇GDT中的第4個描述符

    - 12345678h 為 Offset 

      若GDT第四個描述符中描述的段基址（Base）為11111111h，則線性地址=11111111h + 12345678h = 23456789h

## Segment Descriptor 段描述符

- 在 GDT 中的段描述符很多種，LDT-Descriptor 只是其中一種，例如
  - 系統段描述符
  - 共享代碼段的描述符
  - 共享數據段的描述符
  - 共享棧段的描述符
  - LDT-Descriptor，用於描述 `LDT 的位置`，用於描述應用程式中的 `代碼段、數據段、堆棧段` 等位置
  
- 無論是哪一種段描述符，都具有相同的格式，依照段描述的設定值，決定是哪一種段描述符

- GDT 中的每個段描述符占用 8 byte，因此，每 8byte 就是一個段描述符的入口位址

  - 注意，intel 規定 ，GDT 的第一個段描述符必須是 null-descriptor

    <img src="protected-mode/pm-7-entry0-must-null.png" width=200 height=auto>

  - GDT 包含第一個 Null 描述符，總共有 8192 個描述符，每個 8 byte

    - 總共占用 8192 * 8 = 65536 個 byte (0x10000)
    - 因為 GDT中的描述符必須透過段選擇子來選擇，段選擇子為 13 bit = 2<sup>13</sup> = 8192

- Segment Descriptor 段描述符 的內容，
  
  在`不同類型`的段，Segment Descriptor 描述的屬性略有不同

  有數據段、代碼段、系統段、三種類型 (棧段屬於數據段)
  
  <img src="protected-mode/pm-11-diff-type-seg-descriptor.png" width=500 height=auto> 

  <font color="blue">以數據段為例</font>

  <img src="protected-mode/pm-1-GDT-format.png" width=600 height=auto>

  | 位置      | 長度  | 符號  | 意義                                             |
  | --------- | ----- | ----- | ------------------------------------------------ |
  | bit 0-15  | 16bit | Limit | 段界限的 bit 0-15 (段大小的低16位)               |
  | bit 48-51 | 4bit  | Limit | 段界限的 bit 16-19  (段大小的高4位)              |
  | bit 16-39 | 24bit | Base  | 段基址的 bit 0-23，內存起始位址低24位            |
  | bit 56-63 | 8bit  | Base  | 段基址的 bit 24-31，內存起始位址高8位            |
  |           |       |       | <font color="blue">bit 40-47，存取控制</font>    |
  | bit 40    | 1bit  | A     | 已存取控制位，                                   |
  | bit 41    | 1bit  | R/W   | 數據段的 Write 控制 / 代碼段的 Read 控制         |
  | bit 42    | 1bit  | E/C   | 數據/棧段的擴展方向 / 指示代碼段是否是共享代碼   |
  | bit 43    | 1bit  | Ex    | Executable，用於判斷當前段是數據段或代碼段       |
  | bit 44    | 1bit  | S     | Descriptor 的類型，系統段或非系統段              |
  | bit 45-46 | 2bit  | DPL   | Descriptor-Privilege-Level，描述符的特權等級     |
  | bit 47    | 1bit  | P     | Present，段是否出現在記憶體中，段描述符是否有效  |
  |           |       |       | <font color="blue">bit 52-55，Flag</font>        |
  | bit 52    | 1bit  | AVL   | Available，供系統程式自由使用                    |
  | bit 53    | 1bit  | L     | Long-mode，指示是否是64位代碼段                  |
  | bit 54    | 1bit  | D/B   | 代碼段/棧段/數據段的位址或操作數的有效長度       |
  | bit 55    | 1bit  | G     | Granularity，段界限的單位(粒度)，G=0=1B，G=1=4KB |
  
- `bit 47`，P，Present，判斷段描述符是否有效
  - P=0，段未出現在記憶體中，段描述符無效，當前的段(記憶體範圍)出現在硬碟上，用於內存映射
  - P=1，段已出現在記憶體中，段描述符有效，出現後，才可以進行位址轉換
  - 見 [分頁架構](https://www.csie.ntu.edu.tw/~wcchen/asm98/asm/proj/b85506061/chap2/paging.html)
  
- `bit 16-39/bit 56-63`，Base，LDT 或 段的基址

- `bit 0-15/bit 48-51`，Limit，段界限，表示最大可循址範圍，搭配 bit-55-G 使用
  - 段界限是用来限制段内偏移地址的，段內偏移地址必須位於段描述符給出的範圍之內，否則CPU會拋出異常
  - 段的界限常用段的大小來表示，段的界限 = (段的大小 * 粒度) -1

  - Limit 共占用 20位，但使用時需要 32位，不足的部分，需要透過 G 來決定如何充填
  - 若 Limit=20單位，G=0=1byte，代表 GDT 的大小為 20*1=20 byte，最大可定址到 1MB
  - 若 Limit=20單位，G=1=4Kbyte，代表 GDT 的大小為 20*4k=80K byte，最大可定址到 4GB

  
- `bit 55`，G，Granularity，粒度指示，指示段大小(Limit)的計量單位，
  - Limit 為 20 bit，Limit 最大值為 0b1111_1111_1111_1111_1111 = 0xFFFFF

  - G=0，計量單位為 1 Byte，Limit 的範圍為 (1 * `1B`) 到 (2<sup>20</sup> * `1B`) = `1B ~ 1MB` 
  
  - G=1，計量單位為 4K Byte，Limit 的範圍為 (1 * `4KB`) 到 (2<sup>20</sup> * `4KB`) = 4KB 到 4 * 2<sup>30</sup> = `4KB ~ 4GB`
  
  - 若 Limit = 0xFFFFF 時
    - G=0，段的大小(Limit)為 0x000_FFFFF 的大小 (在 limit 前方加3個0)，共 32bit
    - G=1，段的大小(Limit)為 0xFFFFF_FFF 的大小  (在 limit 後方加3個F)，共 32bit

- `bit 44`，S，Descriptor 的類型
  - S=0，Descriptor 為系統級別的段
    注意，GDT 不屬於系統段，GDT 指向 LDT 的位址，而不指向實際的數據或代碼
  - S=1，Descriptor 為 代碼段或數據段

- `S=0`，透過 bit40-43 可以指定不同類型的`數據段/代碼段`
  
  <img src="protected-mode/pm-3-bit40-bit43-type-s0.png" width=600 height=auto>

  `S=1`，透過 bit40-43 可以指定不同類型的`系統段` (A/RW/EC/Ex 無效)

  <img src="protected-mode/pm-3-bit40-bit43-type-s1.png" width=600 height=auto>

- `bit 40`，A，Accessed，已存取控制位
  - 需要初始化為0，當CPU存取過 segment 後，會自動設置為1，
  
  - 用於統計當前段描述符的被訪問頻率，
    - 頻率越高者，留在內存的機率越高，
    - 頻率低者，可以在虛擬硬碟換入換出，轉移到硬碟上
  
  - 可以用在虛擬記憶體管理中，判斷一個 segment 是否需要更新，也可以用來做 debug 的用途
  
- `bit 41`，R/W，段的讀寫控制位
  - 對於數據段，RW 為寫入控制 (W)，W=0，不允許寫入段，數據段的讀取不受限制
  - 對於代碼段，RW 為讀取控制 (R)，R=0，對OS或App而言，不允許讀取該代碼段的程式，但代碼段的執行不受限制
  
- `bit 42`，E/C，E=Expand-Direction=數據段/棧段的擴展方向，C=Conforming=是否是共享代碼
  - 對數據段，用 E 判斷段的擴展方向
    - E=0，可存取區域在limit之上，代表 base ~ base + limit 之間的區域有效，其餘區域無效，`用於數據段`，從低地址到高地址

    - E=1，可存取區域在limit之下，代表 base ~ base + limit 之間的區域無效，limit 以下的區域有效，`用於棧段`，從高地址到低地址

    - 圖解擴展方向

      <img src="protected-mode/pm-8-expand-direction.png" width=300 height=auto>

  - 對代碼段，用 C 判斷代碼段是否是共享代碼段
    - 什麼是一致代碼段，作業系統拿出來被共享的代碼段，`可以被低特權級的用戶直接調用訪問的代碼`
      通常具有部存取受保護的資源和某些類型異常處理的特性，例如，數據計算庫

    - 一般代碼段的限制
      - 只允許同級間訪問
      - 絕對禁止不同級訪問: 核心態不用用戶態.用戶態也不使用核心態
      - 通常低特權代碼必須通過"門"來實現對高特權代碼的訪問和調用

    - 一致代碼段(共享代碼段)的限制，
      - 特權級高的程序不允許訪問特權級低的數據，例如，Ring3不允許調用Ring0的數據
      - 特權級低的程序可以訪問到特權級高的數據，且權限不會發生切換
    
    - C=0，當前的段不是一致代碼段 (不是共享代碼)
    - C=1，當前的段是一致代碼段 (是共享代碼)
  
    - 見 [特權等級](https://www.csie.ntu.edu.tw/~wcchen/asm98/asm/proj/b85506061/chap3/privilege.html)

- `bit 43`，Ex，Executable，用於判斷是數據段或代碼段
  - Ex=0，不可執行，代表當前段為數據段
  - Ex=1，可執行，代表當前段為代碼段
  
- `bit 54`，D/B，代碼段/棧段/數據段的位址或操作數的有效長度
  - D/B 的設置，使得 32bit 的 CPU 可以兼容 16bit 的保護模式
  
  - 對代碼段，
    - D=0，使用16位地址和16/8位操作數，代碼段使用16bit的ip暫存器，
    - D=1，使用32位地址和32/8位操作數，代碼段使用32bit的eip暫存器
  
  - 對棧段，
    - B=0，棧使用16位操作數，堆棧指針用SP，最大記憶體地址為 0xFFFF
    - B=1，使用32位操作數，堆棧指針用ESP，最大記憶體地址為 0xFFFFFFFF

  - 對向上擴展的數據段無效

    向上擴展的數據段已經有 limit 限制其大小

  - 對向下擴展的數據段，指定數據段的上限
    - B=0，數據段的上限為 64KB
    - B=1，數據段的上限為 4GB
    - 圖解
  
      <img src="protected-mode/pm-10-db-for-data-segment.png" width=300 height=auto>
  
- `bit 45-46`，DPL，描述符特權等級，規定了訪問該段所需要的特權級別是什麼
  - DPL = Descriptor-Privilege-Level 或 Demand-Privilege-Level 需求等級
    
    請求者的特權級別(Request-Privilege-Level)，在段選擇子(Selector)的RPL位設定

  - 特權級別的數值越小，權力越大
  - DPL = 0 = 需要最高權限（內核）才能存取段
  - DPL = 3 = 需要最低權限（用戶應用程序）才能存取段
  
- `bit 52`，AVL，availiable，由系統軟件自由使用，CPU 不使用
  
- `bit 53`，L，Long-mode code flag，指示是否是64位代碼段
  - 來設置是否是64位代碼段
  - L=0，Descriptor 描述的是其他長度的代碼段
  - L-1，Descriptor 描述的是 64bit 長度的代碼段

## LDTR 暫存器，描述 LDT 的位置和大小

- LDTR暫存器 的功能和 GDTR暫存器相似，不同的是
  - GDTR暫存器，48bit，保存GDT的基址和限長，
  - LDTR暫存器，16bit，保存`LDT-Descriptor 的內容`，包含LDT的基址、限長和<font color=red>段選擇子</font>

- LDTR暫存器 相關指令
  - `LLDT 指令`，Load LDT，用於將LDT-Descriptor加載到LDTR暫存器中
  - `SLDT 指令`，Store LDT，將 LDTR暫存器的段選擇器，保存在目標操作數中

  - 在代碼中，需要建立手動`建立LDT的內容`，建立後，透過`SLDT指令`保存在`LDTR暫存器`中，再透過`LGDT指令`，將GDT的內容加載到`內存`


## 保護模式下，存取實際物理位址的過程 (未開啟分頁機制的假設下)

- 圖解流程

  <img src="protected-mode/pm-12-flow.png" width=900 height=auto>

- 範例，存取LDT2中，第三個描述符所描述的段的地址，
  
  例如，存取 第三個描述符 的 12345678h 為例
  
- step1，透過 LLDT指令，將當前GDT中的 LDT-Descriptor 載到 LDTR暫存器中
  
- step2，若邏輯地址為 1C:12345678h
  - Selector = 1C = 0b0001_1100，得到 RPL=0，TI=1 選擇 LDT，Index=0b11=3，代表存取 LDT2
  - OFFSET = 0x12345678

- step3，根據 LDTR暫存器的內容，得到 LDT 的基址為 0x11111111h，加上 step1 中 OFFSET 為 0x12345678
  - 3-1，得到 LDT 在線性地址空間的位址為 0x11111111h + 0x12345678 = 0x3456789
  - 3-2，根據 段選擇器(CS或DS或SS)的內容，得到要存取的記憶體位址的偏移位址
  - 實際要存取的地址 = `( 3-1，LDT的基址0x3456789 ) + ( 3-2，偏移位址)` 

## 進入保護模式的步驟

- step1，關閉中斷
  
  因為保護模式的中斷尚未初始化，在初始化前先禁止中斷，執行 `cli` 

- step2，開啟 A<sub>20</sub>
  
  - 在8086/8088中，記憶體空間為 1M (2<sup>20</sup>)，只有20位地址線來定址，即A<sub>0</sub>-A<sub>19</sub>，若內存超過1MB，需要第21根位地址線(A<sub>20</sub>)的支持。

    - 8086 透過 `段寄存器`，虛擬出第21根位地址線(A<sub>20</sub>)，
      
      `段寄存器 << 4 + Offset`

    - 80286，16M (2<sup>24</sup>) 內存，總共 24根地址線 (A<sub>0</sub>-A<sub>23</sub>)
    
    - 80386，4G (2<sup>32</sup>) 內存，總共 32根地址線 (A<sub>0</sub>-A<sub>31</sub>)
    
  - 8086 的地址繞回
  
    由於 8086 的實模式下的地址線是20位，最大尋址空間為0x00000~0xFFFFF，超出1MB的部分在邏輯上也是正常的，
    
    CPU會將超出 1M 的部分，自動迴繞到0地址，相當於進行 MOD 運算 (將地址除 1M 後取餘數)，例如，1M 最大的存取地址為 0x0 - 0xFFFFF，總共 0x100000 的位址，
    
    若地址為 0x100003 / 0x100000 = 1 ... 3，超過 1M 空間，地址會從頭計算，指向 0x3 的位址

  - 為了兼容 8086，需要手動將 A20 打開，才能存取超過 1M 的記憶體位址

    80286/80386 以後的CPU 雖然地址線已經多出很多，但為了兼容 8086，預設會將 A20 關閉，使記憶體循址進行地址繞回，此時，只能存取 1M 的記憶體空間，若要`存取超過 1M`，就需要`重新將 A20 打開 (A20-Gate)`，才能關閉地址繞回的特性，存取超過 1M 記憶體空間

  - A20-Gate 位於 0x92 端口
    - 0x92端口，系統用的控制端口A (System-Control-PortA)，
    - 0x61端口，系統用的控制端口B (System-Control-PortB)，
    - 0x92端口的 bit1，用於 A20 的開關

      <img src="protected-mode/pm-13-0x92-a20gate.png" width=500 height=auto>

    - 詳見，[A20](https://wiki.osdev.org/A20) 和 [0x92](https://wiki.osdev.org/Non_Maskable_Interrupt)

  - 開啟 A20 的範例代碼
    ```asm
      in al,0x92
      or al,0000_0010b
      out 0x92,al
      cli    ;禁止中斷
    ```
- step3，載入 GDT
  ```asm
  lgdt [ 指向gdt的指針 ]
  ```
- step4，打開保護模式，將 CR0暫存器的 `bit-0-PE位` 設置為1
  ```asm
    mov eax,cr0
    or eax,1
    mov cr0,eax
  ```

- step5，透過 `jmp dword` 進行 32bit 的長跳轉
 
  `jmp dword <選擇子>:<32bit代碼段位址>`

## 測試保護模式

- 保護模式下，出現 segment fault 的原因
  - 放問了不該訪問的暫存器，部分暫存器只能被系統訪問

    例如，在應用程式中訪問 cr0 暫存器

  - 訪問的內存超過段描述符允許的範圍
  - 調用了 in / out 指令
  - 超過權限，RPL 和 DPL 不匹配
    - 應用程式的權限最低，Privilege-Level = 3
    - OS的權限最高，Privilege-Level = 0

- 驗證權限的時機
  - 請求的權限 (RPL) 會放在段選擇子的 bit0-1，
  - 段的存取權限 (DPL) 會放在段描述符的 bit45-46，
  - 檢驗的時機，會在`執行LLDT指令`的時候進行驗證

- 若段描述符 gdt_test 規定的存取範圍為 0x10000 ~ 0x10fff

  以下的代碼就會造成跨界限的存取

  `mov word [1:0x2000], 0x55aa`

  從 bochs 的控制台就可以看到 <font color=red> write_virtual_checks(): write beyond limit, r/w </font> 的訊息

  <img src="protected-mode/pm-14-cross-limit-access.png" width=auto height=auto>

- 應用程式的越界測試

  <img src="protected-mode/pm-15-cross-limit-access-in-app.png" width=500 height=auto>

## 參考
- [全局描述符表和局部描述符表](https://zhuanlan.zhihu.com/p/353688030)
- [Intel Architecture 保護模式架構](https://www.csie.ntu.edu.tw/~wcchen/asm98/asm/proj/b85506061/cover.html)
- [IA-32 架構軟件開發人員手冊 vol3](https://zh.1lib.tw/book/11918325/1f16c9)
- [GDT @ osdev](https://wiki.osdev.org/Global_Descriptor_Table)
- [保護模式 @ 羽夏](https://www.cnblogs.com/wingsummer/p/15308064.html)
- [保護模式 @ chenhh](https://github.com/chenhh/cpp-system-dev/tree/master/assembly)