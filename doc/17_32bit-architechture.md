## 32位元架構概述

- 在 bootloader 和 loader 下都是實模式(real-mode)，

  - 實模式(real-mode)，代表可以存取內存的任意位置，不會有任何限制，容易產生資料洩漏或病毒感染
  - 保護模式(protected-mode)，代表可以存取內存是有限制的，無法隨意訪問

- 歷史
  - 1982年，intel 在 80286 引入了 24bit 的保護模式
  - 1985年，intel 在 80386 引入了 32bit 的保護模式 (兼容機)
  - 1989年，intel 在 80486 引入了 內存分頁的機制

- 默認使用 32bit 的數據，例如，push eax 

## 32位元架構的暫存器

- 8086，`16bit` 的暫存器，
  - 通用暫存器，ax、bx、cx、dx、sp、bp、si、di
  - 特殊暫存器，ip、flag
  - 段暫存器，CS、DS、ES、SS、
  
- 80386，`32bit` 的暫存器，
  - e 代表 extened，表示 32bit 的暫存器
  - R 代表 Register，表示 64bit 的暫存器
  
  - 通用暫存器，eax、ebx、ecx、edx、esp、ebp、esi、edi

    注意，可以單獨訪問低16位或高8位或低8位，但不能單獨訪問 高 16位
    ```
    0x0000_0000
      ^^^^      高16位不可單獨訪問
           ^^^^ 低16位可單獨訪問
           ^^   高8位可單獨訪問
             ^^ 低8位可單獨訪問
    ```

  - 特殊暫存器，eip、eflag
  - 段暫存器，CS、DS、ES、SS、FS、GS (維持 16bit，但新增兩個段暫存器)
  - gdtr / idtr 暫存器
  - cr0 / cr2 / cr3 / cr4 控制暫存器
  - efer 暫存器
  
## 32位元架構的循址方式

- 8086 的循址方式
  - BX/BP + SI/DI + offset常量
- 80386 的循址方式
  - (8個通用暫存器其中一個) + (8個通用暫存器其中一個) * (1/2/4/8) + offset常量
  - 32bit 不限制使用的暫存器

## 以 asm 編寫 32bit可執行程式

- 環境
  - archlinux
  - 安裝 gcc 和 32bit 相關的庫
    pacman -S `base-devel` `lib32-gcc-libs`

- 編寫應用程式基礎
  - 必需要有 main 的`入口函數`，且透過 `global main` 將 main 的符號導出
  - 透過 `[bits 位元數]`，可以指名應用程序的位元數
  - 透過 `section`，可以區分程式區和數據區

- 編譯命令
  - 透過 nsam 編譯為 *.o 檔
    > nasm -f elf32 hello.asm
  - 透過 gcc 連結為 可執行檔
    > gcc -m32 hello.o -o hello -static
  - 執行
    > ./hello

## 調用外部函數

- step1，需要用 extern 指名為外部函數
- step2，調用外部函數前，需要將函數參數壓入棧中
- step3，透過 call 指令進行調用

  <img src="32bit-architechture/32-1-extern-function.png" width=400 height=auto>

## 調用 system-call 函數

- 什麼是系統調用
  
  linux內核中設置了一組用於實現系統功能的子程序，稱為系統調用。

  系統調用和普通庫函數調用非常相似，只是系統調用`由操作系統核心提供`，運行於內核態，而普通的函數調用`由函數庫`或`用戶`自己提供，運行於用戶態。

- 透過 int 0x80 進行調用
- 調用前，需要將參數放入暫存器中
- 系統調用的第一個參數，必定是指名要執行的`系統函數的編碼`

  例如，system-call 的 exit() 的編碼為 1

  ```asm
  mov eax, 1    ; 調用 system-call 的 exit()
  mov eax, 0    ; exit() 的第1的輸入參數，status-code
  int 0x80      ; 執行 exit()
  ```

  可以透過 `cat /usr/include/asm/unistd_32.h` 查看系統調用的函數號

  <img src="32bit-architechture/32-2-system-call-function.png" width=400 height=auto>