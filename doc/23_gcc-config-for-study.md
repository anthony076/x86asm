## 學習用的 gcc 編譯選項 

- 以 hello.c 為例
  ```c
  #include <stdio.h>

  int main(){
    printf("hello\n");
  }
  ```

- 編譯命令
  
  `gcc -m32 -S src/hello.c -o build/hello.s`

- 去除`調用棧回朔訊息` (cfi指令)
  - 代碼
  
    <img src="gcc-config-for-study/gc-1-cfi-instruction.png" width=300 height=auto>
  
  - 作用

    CFI = Call-Frame-Information，調用棧祯數訊息，可以在調用失敗的時候回朔棧

  - 去除方式，添加 `-fno-asynchronous-unwind-tables`

- 去除 `PIC 相關指令`
  - 代碼
  
    <img src="gcc-config-for-study/gc-2-got-instruction.png" width=300 height=auto>
  
  - 作用

    PIC，Position-Independent-Code，用於產生與位置無關代碼，
    用於動態連結，動態連結庫只有在程序載入時，才會確定引用動態連結庫的變數和函數的記憶體位置，

    因為 代碼段在編譯後就不能修改，而動態連結庫在編譯期又不會確定記憶體位址，
    且 80386 中，不允許 `mov eax, eip` 的指令，因此必須改用`函數調用`的方式獲取動態的記憶體位址

    因此，動態連結庫的引用，在編譯期時會改用位置無關代碼來取代，利用函數調用的方式，會從全局偏移表(Global-Offset-Table)中獲取 eip 的值

  - 去除方式，添加 `-fno-pic`

- 去除 `棧對齊指令`
  - 代碼

    <img src="gcc-config-for-study/gc-3-stack-align-instruction.png" width=300 height=auto>

  - 去除方式，添加 `-mpreferred-stack-boundary=2`


- 將 AT&T 格式轉換為 INTEL 格式
  
  添加 `-masm=intel`

- 最後的編譯命令和產生的代碼
  - 編譯命令
    ```
    gcc -m32 \
      -fno-asynchronous-unwind-tables \
      -fno-pic \
      -mpreferred-stack-boundary=2 
      -masm=intel \
      -S src/hello.c -o build/hello.s 
    ```

  - 編譯後的 hello.s
    ```asm
    main:
      push	ebp     
      mov	ebp, esp  ; 保存棧頂指針，函數返回用

      push	OFFSET FLAT:.LC0  ; 取得 hello 字串
      call	puts              ; 調用 c語言內建的 puts 函數來顯示字串
      add	esp, 4  ; 完成調用後恢復棧指針

      mov	eax, 0  ; 函數返回值，規定放在 eax

      leave         ; gcc 的精簡指令，等校於
                    ; mov esp, ebp
                    ; pop ebp
      ret
    ```


