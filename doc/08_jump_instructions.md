
## 轉移指令

- 8086 的轉移指令，需要用到 cs暫存器 和 IP暫存器，由 `cs:ip` 組合成`下一條指令`的物理記憶體位址
  
  下一條指令的物理記憶體位址 = `cs * 16 + ip` ( 或 cs << 4 + ip )
  
- cs暫存器 和 IP暫存器的值，`不能透過手動`的方式直接修改，必須透過跳轉指令

- jump 的`偏移量是負數`，並且以`補碼`的方式表示
  
  計算偏移量時，`偏移量 = 目標位址 - 當前 jmp 指令的位址`，例如，

    ```asm
    0x7c50  start:
    0x7c52  jmp short start     ; 會被編譯為 0xEB 0xFE

    ```
    目標地址為 0x7c50，當前 jmp 指令的位址為 0x7c52，偏移量為 0x7c50 - 0x7c52 = -2

    -2 改成補碼形式為 0xFE 

- 8086 的物理記憶體位置的計算方式為，`段地址 * 16 + 偏移地址`
  
  偏移地址為 2byte 的暫存器所保存，在不改變段地址的情況下，偏移地址可以定位 0 - FFFF 的範圍 (64K)

  因此需要 `jmp far 指令`，進行大範圍的跳轉， `jmp far 指令` 會修改段地址，以實現大範圍的跳轉

- jmp short 和 jmp near 適用於 64K 範圍內的跳轉，`不會`修改段地址
  
  jmp far 適用於 64K 範圍外的跳轉，`會修改段地址`

  跳轉範圍，`jmp short` < `jmp near` < `jmp far`

- 轉移指令是條件判斷、函數調用、中斷、異常 的基礎
  
  | 使用場景 | 如何利用跳轉                             |
  | -------- | ---------------------------------------- |
  | 條件判斷 | 利用`旗標`來判斷是否進行跳轉             |
  | 函數調用 | 利用 `stack` 來進行跳轉                  |
  | 中斷     | 中斷觸發後，跳轉到中斷向量表的記憶體位置 |
  | 異常     | 中斷的一種，由系統自動觸發中斷           |


## 用補碼表示 jump 的偏移量
- 見 onenote 的 <a href="https://onedrive.live.com/view.aspx?resid=4A8B4E8C5D69BD67%214703&id=documents&wd=target%28%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80%E7%9B%B8%E9%97%9C%2F%E4%BB%A3%E7%A2%BC%E7%9B%B8%E9%97%9C.one%7C400BDC15-AEE2-4B55-A098-531A3C3038E3%2FMy_%E8%A3%9C%E7%A2%BC_%E5%88%A9%E7%94%A8%E5%8A%A0%E6%B3%95%E5%99%A8%E5%AF%A6%E7%8F%BE%E6%B8%9B%E6%B3%95%E5%99%A8%7CF6BAE894-B127-4B16-BCE0-69181FB23C97%2F%29
onenote:https://d.docs.live.net/4a8b4e8c5d69bd67/文件/待整理/程式語言相關/代碼相關.one#My_補碼_利用加法器實現減法器&section-id={400BDC15-AEE2-4B55-A098-531A3C3038E3}&page-id={F6BAE894-B127-4B16-BCE0-69181FB23C97}&end"> 補碼_利用加法器實現減法器 </a>

- 取得負數的補碼形式，`補碼 = 原碼取反 + 1` 或 `補碼 = | 最大值 - 原碼 |`
  
  例如，0x1 的補碼為 0xFF

  | 計算過程   | 值          |
  | ---------- | ----------- |
  | 原碼 0x1   | 0x0000_0001 |
  | 原碼取反   | 0x1111_1110 |
  | 原碼取反+1 | 0x1111_1111 |

- 利用補碼進行負數的加法時，`產生的進位可忽略不計`，進位是補碼的特性所致，減法不會超過產生進位

## jmp short <標記> 指令
- 編譯後的二進制指令會占用 `2byte`，指令 1byte + 偏移量 1byte
  
  1byte 最大值為 0xFF (0d256)，可顯示的範圍為 -128 ~ 127

- 例如，以下的指令，會被編譯為 0xEB 0xFE

    - 0xEB 為 jmp
    - 0xFE = -2

    ```asm
    0x7c50  start:
    0x7c52  jmp short start     ; 會被編譯為 0xEB 0xFE

    ```
    - 因為 jmp short <標記> 的指令占用 2byte，
     
      執行 0x7c52 的 jmp 指令後，會將 cs:ip 的值 -2，回到 0x7c50 的位置

## jmp near <標記> 指令
- 編譯後的二進制指令會占用 `3byte`，指令 1byte + 偏移量 2byte

  2byte 最大值為 0xFFFF (0d65535)，可顯示的範圍為 -32768 ~ 32767

  
- 例如，以下的指令，會被編譯為 0xE9 0xFB 0xFF
    - 0xE9 為 jmp
    - 0xFB 0xFF = -5
  
    ```asm
    0x7c50  start:
    0x7c52  jmp near start     ; 會被編譯為 0xE9 0xFB 0xFF

    ```
    - 因為 jmp short <標記> 的指令占用 2byte，
     
      執行 0x7c52 的 jmp 指令後，會將 cs:ip 的值 -2，回到 0x7c50 的位置

## jmp far <CS值>:<IP值> 指令，
- jmp far 可以指定段地址，直接指定CS:ip的位置
  - 方法1，(常用) 直接指定位址，此方法可省略 far 關鍵字，例如，
  
    `jmp 0x0:0x7c00`
  
  - 方法2，(不常用) 間接定址，透過內存指定位址，例如，
    ```asm
    jmp far [start]
    start: 
        dw 0x7c00, 0x0  ; 注意，先填入偏移地址，在寫段地址

    ```

  
- 編譯後的二進制指令會占用 `5byte`，指令 1byte + cs 1byte + ip 3byte

  3byte 最大值為 0xFF_FF_FF (16,777,215)，可顯示的範圍為 -8,388,607 ~ 8,388,606

  例如，jmp 0x0:0x7c00 會被編譯為 `0xEA 0x00 0x7c 0x00 0x00`

    - 0xEA 為 jmp 的二進制碼，
    - 0x00 為 CS 值，
    - 0x7c 0x00 0x00 為 IP 值

## jz 指令，條件跳轉指令
- jz 是條件跳轉指令，根據ZF的結果進行跳轉的判斷，ZF=0時才進行跳轉
- 範例
  ```asm
    add ax, cx 
    sub cx, 1
    jz end     ; 執行完 sub 指令後，判斷 ZF 的值是否為 0
    jmp start
  ```


## CMP 指令，條件跳轉指令
- CMP 常用於條件跳轉，用於更新 ZF 值，但`不進行跳轉`
- 範例
  ```asm
  mov ax, 5  
  mov bx, 5
  cmp ax, bx
  ```

## 條件轉移指令

| 比較結果   | 英文                 | 指令 | 標誌                  |
| ---------- | -------------------- | ---- | --------------------- |
| 等於       | Equal                | je   | ZF=1                  |
| 不等於     | Not Equal            | jne  | ZF=0                  |
| 大於       | Greater              | jg   | ZF=0 and SF=0F        |
| 大於等於   | Greater or Equal     | jge  | SF=0F                 |
| 不大於     | Not Greater          | jng  | ZF=1 or SF $\neq$ 0F  |
| 不大於等於 | Not Greater or Equal | jnge | SF $\neq$ 0F          |
| 小於       | Less                 | jl   | SF $\neq$ 0F          |
| 小於等於   | Less or Equal        | jle  | ZF=1 and SF $\neq$ 0F |
| 不小於     | Not Less             | jnl  | SF=0F                 |
| 不小於等於 | Not Less or Equal    | jnle | ZF=0 and SF = 0F      |
| 高於       | Above                | ja   | CF=0 and ZF=0         |
| 高於等於   | Above or Equal       | jae  | CF=0                  |
| 不高於     | Not Above            | jna  | CF=1 or ZF=1          |
| 不高於等於 | Not Above or Equal   | jnae | CF=1                  |
| 低於       | Below                | jb   | CF=1                  |
| 低於等於   | Below or Equal       | jbe  | CF=1 or ZF=1          |
| 不低於     | Not Below            | jnb  | CF=0                  |
| 不低於等於 | Not Blow or Equal    | jnbe | CF=0 and ZF=0         |
| 偶         | Parity Even          | jpe  | PF=1                  |
| 奇         | Parity Odd           | jpo  | PF=0                  |
