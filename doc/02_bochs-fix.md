## Issue
- Bochs `v2.6.11` 進入模擬後，點擊 View -> Stack 後，GUI 會阻塞無反應
  
  <img src="bochs-bug/bochs-bug-issue.png" width=500 height=auto>
  
  需要手動對 v2.6.11 重新編譯，並使用 patch 進行修正
  
  <img src="bochs-bug/bochs-bug-patch.png" width=500 height=auto>

- Bochs `v2.7.1` 已修正此問題，點擊 View -> Stack 後不會阻塞無反應

## 在 archlinux 上重新編譯 bochs v2.6.11
- 將 `bochs-build 目錄`中的 `PKGBUILD` 和 `fix-build.patch` 複製到VM中
  
  注意，不需要手動下載 bochs v2.6.11，PKGBUILD 會自動下載 bochs v2.6.11 的源碼

- 將已安裝的 bochs 移除

    `sudo pacman -R bochs`

- 執行 `makepkg -si`

- 或直接使用已重新編譯的 docker image
  - dockerfile: [dockerfile](../docker-bochs/Dockerfile)
  - pull: `docker pull chinghua0731/arch-bochs:latest`
  - 執行: `docker run -it --rm chinghua0731/arch-bochs bash`
