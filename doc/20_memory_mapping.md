## 映射的功能
- CPU 進入`保護模式`後，要存取記憶體必須經過 GDT 將邏輯地址(logic-address)轉換為線性地址 (linear-address)，
  
  - 若`有啟動分頁機制`，會進一步將線性地址轉換為實際的物理地址
    ```mermaid
    flowchart LR
    邏輯地址 --> GDT --> 線性地址 --> 分頁機制 --> 物理地址
    ```
  - 若`沒有啟動分頁機制`，線性地址就是實際的物理地址
    ```mermaid
    flowchart LR
    b1(邏輯地址) --> b2(GDT) --> b3(線性地址\n 物理地址 )
    ```
  - 邏輯地址也稱為虛擬地址，邏輯地址組成的記憶體空間，又稱為虛擬記憶體

- `映射功能1`，用於 SWAP，用硬碟模擬內存，增加內存容量
  - 記憶體的空間有限，當`記憶體空間的資源不足`時，原本儲存在記憶體中的數據，就可以轉存在硬碟空間中，
  
  - 硬碟和記憶體都是用來儲存數據的，只是硬碟的存取速度相較記憶體慢得多，因此，只會將不常用程式的虛擬空間，轉存到一個專門用於`暫存內存相關數據`的硬碟空間，稱為 SWAP 交換區
  
  - 當需要SWAP區的程序時，就從SWAP區重新載入到記憶體，否則它不會主動交換到真實記憶體中

  - 虛擬記憶體因為具有比較大的空間，因此可以實現 SWAP 的功能，透過`虛擬記憶體的映射`將原本要寫入記憶體的虛擬地址，映射到硬碟的記憶體空間

- `映射功能2`，使訪問同一個虛擬地址的`不同進程`，透過映射`訪問不同的物理地址`，從而避免數據競爭的問題

  例如，進程A和進程B同時都要訪問相同的虛擬內存地址 0x10000，若不進行處理，就會造成存取的衝突，有可能互相覆蓋對方的資料

  利用虛擬內存的映射，就可以做到進程A的虛擬內存地址，實際訪問物理地址 0x10000 的位置，利用內存映射的轉換機制，將進程B的虛擬地址映射到物理地址 0x20000 的位置，映射後的物理地址若超過實際的記憶體大小，就可以透過 SWAP 轉存到硬碟中，實現更大的記憶體空間

  更大的記憶體空間必須透過`虛擬記憶體`定址，再配合`虛擬內存映射`，將資料實際儲存在硬碟上

  透過映射，使得所有程式都可以訪問 4G 的虛擬空間，而不用擔心會存取到相同的記憶體位置，造成數據覆蓋的問題

  | 功能       | 說明                                 |
  | ---------- | ------------------------------------ |
  | 虛擬記憶體 | 更大的定址空間                       |
  | 映射       | 將相同的虛擬地址拆分到不同的物理地址 |
  | SWAP       | 實現更大的物理空間                   |
  

## 內存映射 和 分頁機制
- 分頁機制是位於保護模式的GDT之後，
  - 先經過 GDT 對段訪問權限的篩選後，將邏輯地址轉換為線性地址(虛擬地址)，
  - 再將線性地址(虛擬地址)透過分頁機制的 pde、pte，轉換為實際的物理地址
  - 示意圖
  
    <img src="protected-mode/pm-9-linear-addr-space.png" width=600 height=auto> 
    

- 物理分頁的限制，映射後的`物理內存`必須使用`分頁`，

  - 物理內存以`分頁`為基本單位，映射前，先取得虛擬內存的大小，計算需要的映射後，將映射寫入映射表中

  - 要使用分頁管理，就`需要把物理內存分成大小相同的頁`，頁的最小單位為 4K Byte = 4096 Byte = 0x1000 Byte，
    
    以 4G內存 為例，4G / 4K = 1M 個分頁 = 1048576 個分頁
    - 第 1 個分頁為 0x0 - 0x1000
    - 第 2 個分頁為 0x1000 - 0x2000
    - 第 1M 個分頁為 0xFFFFF000 - 0xFFFFFFFF 
  - 示意圖
  
    <img src="memory-mapping/mm-5-memory-in-page.png" width=400 height=auto>

    從上圖可知，每個頁的`基址`的`低12位`都是0 (偏移量的低12位不為0，但偏移量不是由基址控制，不在討論範圍)

- 映射的目的，將虛擬內存的指定區域，映射到物理內存的分頁中

  <img src="memory-mapping/mm-6-segment-to-page.png" width=700 height=auto>

  - 以上圖為例，一塊 0x200000 - 0x202008 的虛擬內存 (共 0x2008 大小的空間) 映射到物理內存為例
    - 0x2008 = 0d8200 的空間，一個分頁為 4K = 4096 Byte，
      
      因此，8200 / 4096 = 2..8，因此總共需要 3 個物理內存的分頁

    - 物理分頁1，0x0000 - 0x2FFF 的物理分頁，4KB的分頁中，占用4KB的空間
    - 物理分頁2，0x4000 - 0x4FFF 的物理分頁，4KB的分頁中，占用4KB的空間
    - 物理分頁3，0x7000 - 0x7FFF 的物理分頁，4KB的分頁中，占用4KB的空間

- `32bit的虛擬地址` 映射到 `20bit的物理內存`，多出來的高位 12bit，用於選擇 PDE 和 PTE，
  
  - PDE 和 PTE 的作用類似實模式下的段寄存器，用於選擇物理內存的高位，`虛擬地址的OFFSET` 和 `物理內存的OFFSET` 會保持一致，

  - 若將虛擬地址 0xC000000 - 0xC00FFFFF(1M) 映射到 0x0 - 0xFFFFF (1M)的物理空間時，

    若存取虛擬地址 0xc00b8000，實際上是存取 0xb8000 

## Page-Directory頁目錄 和 Page-Table頁表 概述
- 映射表實際上是由以下兩個部分組成
  - Page-Directory，頁目錄
  - Page-Table，頁表 
  
- Page-Directory，頁目錄，紀錄可用的 Page-Table，用於`節省映射表的空間`
  
  PDE，Page-Directory-Entry，頁目錄項，頁目錄的組成項，
  
  見以下 [PD和PDE的詳解]() 的說明
  
- Page-Table，映射表或頁表，用於`虛擬內存`和`物理內存`之間的轉換
  
  PTE，Page-Table-Entry，頁表項，是映射表的組成項，紀錄`分頁屬性`和`映射的物理內存分頁的索引`

  見以下 [PT和PTE的詳解]() 的說明

- 虛擬地址、PDE、PTE 關係
  - `虛擬地址`的
    - 因為物理分頁基址的低12位為零，(見 [內存映射和分頁機制]() 的說明)，可以將虛擬地址分配如下
    - 高10位，bit22-31，用於從 Page-Table 中選擇指定的 PTE，保存 PDE-index
    - 中10位，bit12-21，用於從 Page-Directory 中選擇指定的 PDE，保存 PTE-index
    - 低12位，bit0-11，物理分頁的偏移地址
    - 將 index 轉換為偏移地址，`PDE/PTE 的偏移地址 = index * 4` 
      
      因為 PDE 長度為 32bit，占用 4Byte，因此每個 PDE 相隔 4Byte
  
  - `Page-Directory`的
    - 高20位，bit12-31，被選擇PTE的基址
    - 低12位，bit0-11，PTE的屬性
    - 共占用 4Byte

  - `Page-Table`的
    - 高20位，bit12-31，被選擇物理分頁的基址
    - 低12位，bit0-11，物理分頁的屬性
    - 共占用 4Byte

  - 虛擬地址存放 `index`，PDE/PTE的 `address`位 (bit12-31)，保存下一級的基址
    - index 需要經過轉換 (index * 4) 才能得到 PDE/PTE 的偏移地址
    - PDE 的下一級是Page-Table，因此 PDE 保存 Page-Table 的基址
    - PTE 的下一級是物理內存，因此 PTE 保存物理內存的基址
  
  - 示意圖

    <img src="memory-mapping/mm-3-pde-and-pte.png" width=500 height=auto>

- 每一個進程運作時，皆擁由各自的 page-table，當目前需要切換進程時，os 會告訴 page-table-hardware 需更換成新的 page-table。

- 當進程需要更多的記憶體空間時，系統會找 free-physical-pages 提供該進程存取，且新增 PTE 到該進程專屬之 page-table，
  
  並指向新的physical pages，並設定新增之PTE的後12 bits(flags)。

## 分頁數量計算範例
  - 以簡單的分頁模式為例 (只有 page-table ，沒有 page-directory)
  
  - 映射表(page-table 或稱頁表) 是兩種內存分頁之間的映射

  - 示意圖

    <img src="memory-mapping/mm-1-memory-mapping.png" width=500 height=auto>

  - 記憶體與 swap 之間的記憶體數據交換，是以記憶體的 Page 為單位的，1 page = 4KB
  
    例如，一個 5KB 的檔案將會占用 2 page (8KB) 的交換區空間

  - 32bit 的系統會有 2<sup>32</sup> = 4GB 的內存，而每個內存分頁的大小是 2<sup>12</sup> = 4KB，
    
    若 4G 內存按 Page 分割，可以得到，4G / 4K = 2<sup>20</sup>，總共有  = 1M 個內存分頁

  - 因為所有的應用程序都`可以使用相同的 4G 內存空間`，透過`映射`，就不用擔心會`存取到相同的記憶體位置`，但是需要每個應用程式都有自己的映射表，才能`映射到不同位置`的物理內存

  - 利用簡單的數組，可以紀錄`虛擬內存分頁`和`物理內存分頁`之間的映射關係

  - 映射表占用的內存空間 和 映射表的最大數量
    
    以上述為例，`int page_table[1 << 20]`，代表虛擬內存的映射表會有 1M 個儲存空間，
    
    page_table 中的每個 PTE 長度為32bit，占用 4Byte 的空間   
    因此，1個映射表的總佔用空間為 `(1M個空間) * (每個空間4Byte的大小)` = 4M Byte 的內存空間

    若將 4MB大小的映射表，`以 page 的形式儲存`，總共需要 4M / 4K = 1K，代表一個映射表需要 1024 個分頁來儲存

## Page-Table (PT) 和 Page-Table-Entry (PTE) 詳解
- Page-Table 用於紀錄每個物理分頁的基址和每個分頁的屬性，
 
  Page-Table 是個數組，數組是由多個 Page-Table-Entry (PTE) 組成
  
  以4G內存為例，共有物理分頁有1024個，因此 Page-Table 中就會有 1024 個 PTE，透過邏輯地址的中10位 (bit12-21) 選擇有效的 PTE 後，從 PTE 的高20位就可以得到物理分頁的基址

- PTE 是 Page-Table 的組成要素  
  - 低12位，bit0-11，物理分頁的屬性
  - 高20位，bit12-31，物理分頁的基址

- PTE 的內容
  - 示意圖

    <img src="memory-mapping/mm-2-page-table-entry.png" width=400 height=auto>

  - bit0，1bit，P，Present，是否存在內存中，
    - P=0，在硬碟上或未初始化
  
  - bit1，1bit，R/W，讀寫控制，
    - RW=0，只讀，
    - RW=1，可寫
  
  - bit2，1bit，U/S，訪問的特權級，
    - U/S=0，只允許超級用戶訪問，特權級3的一般用戶不可訪問，
    - U/S=1，允許所有用戶放問
    - U/S=3，禁止訪問
  
  - bit3，1bit，PWT，page-level write-through，決定快取的方式
    - 見以下 [CPU Cache，CPU 與 物理內存之間內容緩衝]() 一節

    - PWT=0，CPU會對這個分頁做 write-back caching，寫Cache的時候就只是寫Cache，是否要映射到內存由CPU緩存控制器自己決定
      > CPU向cache寫入數據時，同時向memory(後端存儲)也寫一份，使cache和memory的數據保持一致。優點是簡單，缺點是每次都要訪問memory，速度比較慢

    - PWT=1，CPU會對這個分頁做 write-through caching，寫Cache的時候也要將數據寫入內存中
      > cpu更新cache時，只是把更新的cache區標記一下，並不同步更新memory(後端存儲)。只是在cache區要被新進入的數據取代時，才更新memory(後端存儲)，
      CPU執行的效率提高，缺點是實現起來技術比較復雜

  - bit4，1bit，PCD，Page-Cache-Disable，是否寫入緩存，直接寫入內存
  
  - bit5，1bit，A，Accessed，是否已經被CPU存取，用於統計頁的使用頻率
  
  - bit6，1bit，D，Dirty，代表該頁正在寫入中，
  
  - bit7，1bit，PAT，Page-ATtribute，頁屬性，設置為0
  
  - bit8，1bit，G，Global，將當前分頁放入全局的映射緩存中
    > 若分頁放入緩存中，就可以直接從 cache 得到分頁的內容
  
  - bit9-11，3bit，AVL，available，供自由使用
  
  - bit12-31，20bit，Address，物理內存分頁的位址


## Page-Directory (PD) 和 Page-Directory-Entry (PDE) 詳解
- 為什麼需要 Page-Directory
  
  實務上，每個進程都有自己的映射表，對一個4G內存來說，每個映射表都需要 4M Byte 的儲存，若是每個進程都保存完整的映射表，對內存來說是不小的浪費，並且，不是每個進程都需要用到完整的 4G 空間，對於未使用的空間，不需要為其建立映射
  
  為了節省內存，引入了`層次化的分頁結構`，即
  ```mermaid
  flowchart LR
  虛擬地址 --> Page-Directory --> Page-Table --> 物理地址
  ```
  
  對於一個層次化的分頁結構，`Page-Directory` 會覆蓋了整個 4GB 的`虛擬地址空間` ( `Page-Table` 會覆蓋了整個 4GB 的`物理分頁空間` )，但如果某個 PDE 未被使用，則該 PDE 指向的 Page-Table 就不需要建立，Page-Table 只有在上層的 PDE `被使用時才需要建立`，

  如果沒有 Page-Directory，`只有 Page-Table` 就做不到這樣的節約內存，對於只有Page-Table的分頁機制，若是虛擬地址在Page-Table中找不到對應的PTE，計算機系統就不能工作，所以 Page-Table 一定要覆蓋全部物理內存，因此無法實現節省內存的目的

- PageDirectory，頁目錄，負責紀錄分頁是否有效，PDE 是頁目錄的組成要素

- PDE 是 Page-Directory 的組成要素  
  - 高20位，bit12-31，PageTable的基址
  - 低12位，bit0-11，PageTable的屬性

- Page-Directory 必須覆蓋整個虛擬地址的空間，Page-Table 必須覆蓋整個物理地址的空間，
  
  因此，PDE 的數量和 PTE 的數量是一致的

- 多層次的分頁結構中，Page-Directory 可以有多個，最後一個是 Page-Table，其他都是 Page-Directory
  
  <img src="memory-mapping/mm-10-multi-page-directory.png" width=800 height=auto>

- PDE的內容，PDE和PTE有類似的結構，用法詳見 PTE

## 虛擬地址轉換為物理地址流程
- CR3 控制寄存器，存放當前任務的頁目錄物理地址
  - CR3示意圖
    
    <img src="memory-mapping/mm-8-page-overview.png" width=500 height=auto>
    
  - 又稱為 頁目錄基址暫存器，Page-Directory-Base-Register，PDBR
  - 當任務切換時，會將`當前任務自己的頁目錄物理地址`傳遞給 CR3，使得CPU切換到新任務開始執行，因此每個任務都只在自己的地址空間執行
  - 頁目錄和頁表也是普通的分頁，和普通分頁址差別在功能不同，

    當任務撤銷後，頁目錄和頁表這兩個分頁，也會和普通分頁依樣被回收

  - CR3 指向頁目錄基址，頁目錄保存了所有頁表的基址，頁表指向物理內存分頁的基址

- 流程圖
  <img src="memory-mapping/mm-7-flow.png" width=700 height=auto>

  - step1，配置 CR3，設置頁目錄的基址
    
  - step2，將 32bit 的虛擬地址取出 `PDE-index(高10bit)`、`PTE-index(中10bit)`、`offset(低12bit)`
    
  - step3，將 PDE-index 透過 index * 4 ，轉換為`PDE的偏移地址`，
    
    `PDE 的實際地址` = `CR3` + `PDE的偏移地址`

  - step4，從 PDE 中取得 Page-Table 的基址
    
  - step5，將 PTE-index 透過 index * 4，轉換為`PTE的偏移地址`

    `PTE 的實際地址` = `Page-Table 的基址` + `PTE的偏移地址`

  - step6，從 PTE 取得 物理分頁的基址
    
  - step7，`物理分頁的基址` + `虛擬地址` = `物理內存的地址`

## 利用 虛擬地址0xFFFFFXXX (XXX為頁目錄的基址)，修改頁目錄的值
- 必要，必須在最後一個 pde的位址中，寫入 `Page-Directory 自己的基址`
  ```asm
  mov eax, PDE 
  or eax, ATTR
  ; 0x3ff 是最後一個 pde 的寫入位置，0b11_1111_1111 = 0x3ff
  mov [PDE + 0x3ff * 4], eax
  ```
- 當 PageDirectory 的基址在物理內存的 0x200 的位置，就可以透過將虛擬地址的高5位設置為F，來修改頁目錄自身的值

- 若虛擬地址為 0xFFFFF200 = 0b1111_1111_1111_1111_1111_0010_0000_0000
  - 高10位 = pde-index = 0b11111_11111 = 0x3ff
  - 中10位 = pte-index = 0b11111_11111 = 0x3ff
  - OFFSET = 頁目錄的基址 = 0b0010_0000_0000 = 0x200

  - 正常狀況下，跳轉到 pde 位址 -> 跳轉到 pte 位址 -> 跳轉到 物理內存地址

    當虛擬地址的高5位為F，且最後一個 pde 的位置已寫入頁目錄基址的狀況下，以上述為例，

    > 因高位的 0x3ff，選中最後一個 pde，跳轉到頁目錄基址，0x200 

    > -> 因中位的 0x3ff，選中最後一個 pde，跳轉到頁目錄基址，0x200 
  
    > -> 跳轉到 0ffset，0x200

    可以看出，整個流程中，都保持在 PageDirectory 的基址，0x200 的位置，實際上並沒有改變位置
  
 
  - 當虛擬地址的高5位為 F 時 (0xFFFFFXXX)，會找到最後一個 pde，因為該 pde 已寫入頁目錄的基址，使德本來應該跳轉到 pte 的位置，轉而跳轉到頁目錄的基址，使得整個解析的過程中，都保持在頁目錄的基址不動

  - 示意圖
    <img src="memory-mapping/mm-9-modify-pde-value.png" width=800 height=auto>

## TLB，translation lookaside buffer，轉譯機制，轉譯的實作
- [TLB實作](https://ithelp.ithome.com.tw/articles/10207851)

## CPU Cache，CPU 與 物理內存之間內容緩衝
- CPU cache 是位於CPU與物理內存之間的臨時存儲器，它的容量比內存小的多，但是交換速度（讀寫速度）比內存要快得多
- CPU cache 是由 PTE 中的 PWT位 和 PCD位 控制
- TLB 和 cache 的差異
  - TLB: 儲存线性地址與物理地址之間的對應關係
  - cache: 存儲了物理地址與内容之間的對應關係
  - 有了CPU緩存，
    
    當CPU再去查找/讀取某一個線性地址對應的物理頁時，就可以先查TLB，找到它的物理地址，再找CPU緩存，找到它的內容

## 參考
- [MTRR](https://www.twblogs.net/a/5b8bdf992b717718832ed51b)
  
- [x86架構下的MTRR](https://blog.csdn.net/jiangwei0512/article/details/100798102)
  
- [PAT詳解](https://blog.csdn.net/arethe/article/details/6238335)

- [PAGE ATTRIBUTE TABLE (PAT) @ intel manual vol3](https://xem.github.io/minix86/manual/intel-x86-and-64-manual-vol3/o_fe12b1e2a880e0ce-445.html)
  
- [Page Tables](https://ntust-csie-islab.github.io/106-OS-homework1/B10415007/)
  
- [Paging @ osdev](https://wiki.osdev.org/Paging)

- [分頁架構](https://www.csie.ntu.edu.tw/~wcchen/asm98/asm/proj/b85506061/chap2/paging.html)

- [PWT 和 PCD](https://blog.csdn.net/qq_41988448/article/details/102763891)

- [page-table 代碼實現](https://ntust-csie-islab.github.io/106-OS-homework1/B10415010/)