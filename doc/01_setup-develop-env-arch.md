## Reference
- https://www.bilibili.com/video/BV1b44y1k7mT

- bochs 配置檔說明

  https://www.itread01.com/content/1547883742.html

## 環境
- Local System: Windows 11
- VMWare WorkStation 16 pro (16.2.2 build-19200509)
- ArchLinux 2022.02.01-x86_64 build

## 安裝
- VM WorkStation 16 Pro，Windows version
  https://www.vmware.com/go/getworkstation-win

- 更新 pacman
  $ sudo pacman -Sy

- $ sudo pacman -S nasm
- $ sudo pacman -S qemu

## 從 源碼建立 Bochs 套件
- step1，將 boch-install 目錄中的 `fix-build.patch` 和 `PKGBUILD` 複製到任意資料夾
  
- step2，
  在使用者目錄下輸入
  
   `[ching@ching src]$ makepkg -si`

   不能在 root 的模式下輸入

   `[root@ching src]#  makepkg -si`
   

## 建立 Bochs 開發環境
- Why Bochs

  Bochs 和 virtualbox、VMware 一樣都是虛擬機，Bochs 提供底層的界面和工具可以用來`調試組合語言`

- step1，建立並編譯 hello.asm

  - hello.asm
    
    ![image](setup/hello.asm.png)

  - 編譯命令
  
    `$ nasm hello.asm -o hello.bin`

- step2，建立 bochs 的設定檔 bochsrc

  `$ bochs -> select 4: create -> input filename，bochsrc -> 7: quit`

- step3，將 bochs 設置為GUI模式

  ![image](setup/configure-gui.png)

- step4，設置 bochs的啟動磁碟

  ![image](setup/configure-hd.png)

- step5，copy `hello.bin` into `master.img`，將 hello.bin 放入磁碟的映像檔中
  - `注意`，使用 dd 前，必須`使用 bximage 建立開機磁區`的內容，否則無法正確開機
  
  - 命令，dd，轉換並複製檔案

    > dd `if`=hello.bin `of`=master.img `bs`=512 `count`=1 `conv`=notrunc

    - 參數，if，input-file，輸入檔案
    - 參數，of，output-file，輸出檔案
    - 參數，bs，read and write up to BYTES bytes at a time，設定扇區大小
    - 參數，count，copy only N input blocks，將輸入檔案的前 N 個磁區複製到輸出檔案
    - 參數，seek，將輸入檔案的數據複製到目標檔案的第N個磁區
    - 參數，conv，convert the file as per the comma separated symbol list，根據逗號分隔的符號列表轉換文件
    
    `注意`，conv 需要設置為 `notrunc`，do not  the output file，不會截斷輸出文件，否則輸出的 master.img 會只有 512 bytes

- step6，利用磁碟映像檔(master.img) 啟動 bochs
  - 輸入 `$ bochs -q` 啟動

    啟動畫面，需要按下 Continue 按鈕，才會開始載入

    ![image](setup/start-bochs-1.png)

    載入後
    
    ![image](setup/start-bochs-2.png)

- step7，利用 qemu-img 命令，將磁碟映像檔(master.img)轉換為 VMware 支持的格式

    `$ qemu-img convert -O vmdk master.img master.vmdk`
  
- step8，在 VMware 中進行測試

  - 建立新的VM

    ![image](setup/test-in-vmware-1.png)

  - 稍後安裝 os

    ![image](setup/test-in-vmware-2.png)

  - 選擇 linux

    ![image](setup/test-in-vmware-3.png)

  - 選擇 virtual disk

    ![image](setup/test-in-vmware-4.png)

  - 選擇 master.vmdk

    ![image](setup/test-in-vmware-5.png)

  - 選擇 keep existing format

    ![image](setup/test-in-vmware-6.png)

  - 啟動後，顯示 hello.asm 中的 T

    ![image](setup/test-in-vmware-7.png)

## 透過指令建立 image
- 參考
  
  [bximage的使用](https://bochs.sourceforge.io/cgi-bin/topper.pl?name=New+Bochs+Documentation&url=https://bochs.sourceforge.io/doc/docbook/)

- bximage 命令會啟動交互來建立 master.img，不利於在 docker 中使用，透過自定義的參數可跳過互動的過程
  
- 範例
  - for bochs v2.7.1
    > bximage -q -hd=16 `-func=create` -sectsize=512 -imgmode=flat ttt.img

  - for bochs v2.6.11
    > bximage -q -hd=16 `-mode=create` -sectsize=512 -imgmode=flat ttt.img
  



## 常用命令列表
- 將asm檔編譯為bin檔

  `nasm hello.asm -o hello.bin`

- 將 bin 檔案的內容複製到磁碟映像檔中

  `dd if=hello.bin of=master.img bs=512 count=1 conv=notrunc`

- 啟動 bochs

  `bochs -q`

- 利用 qemu-img 命令，將 master.img 轉換為 VMware 的虛擬磁碟 (*.vmdk)

  `$ qemu-img convert -O vmdk master.img master.vmdk`