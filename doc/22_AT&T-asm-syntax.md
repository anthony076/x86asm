## 為什麼需要 AT&T

在c語言中可以直接寫 ASM 的代碼 (內聯彙編)，進行內聯彙編時需要 AT&T 的語法，
雖然可以轉換為 INTEL 的語法，但是某些項目可能還是使用at&t語法
  
## AT&T & INTEL 組合語言格式

- 立即數要加上 `$` 
  - AT&T，`movl $0x100, %eax`
  - INTEL，`mov eax, 0x100`

- 暫存器要加上 `%`
  - AT&T，`movl $message, %eax`
  - INTEL，`mov eax, message`

- mov 的傳遞方向相反
  - AT&T，`movl $0x100, %eax`
  - INTEL，`mov eax, 0x100`

- 數據傳送指令需要加上數據長度，movx，x代表數據的長度
  - AT&T，`movx 來源, 目的`，傳遞方向由左向右
    - `movl $0x100, %eax`
    - `movl $0x100, %eax`
  
  - INTEL，`mov 目的, 來源`，傳遞方向由右向左
    - `mov eax, 0x100`

  - 等價比較
    | AT&T | 描述 | Intel     |
    | ---- | ---- | --------- |
    | movl | 32位 | mov dword |
    | movw | 16位 | mov word  |
    | movb | 8位  | mov byte  |

- AT&T 的註解為 `#`

## 不會產出機械碼偽指令

- 偽指令用於提供彙編器特定訊息，不會產生對應的機械指令
    
- 用於數據類型的偽指令

  | 偽命令    | 數據類型                | nasm   |
  | --------- | ----------------------- | ------ |
  | `.ascii`  | 字符串                  | `db`   |
  | `.asciz`  | 以 `\0` 結尾的字符串    | `db 0` |
  | `.byte`   | byte                    | `db`   |
  | `.double` | 雙精度浮點              | `dq`   |
  | `.float`  | 單精度浮點              | `dd`   |
  | `.int`    | 32位整數                | `dd`   |
  | `.long`   | 32位整數和(`.int` 相同) | `dd`   |
  | `.octa`   | 16 byte 整數            |        |
  | `.quad`   | 8 byte 整數             | `dq`   |
  | `.short`  | 16位整數                | `dw`   |
  | `.single` | 單精度浮點              | `dd`   |

- 用於指示功能區段的偽指令
  - 與代碼相關
    - 代碼段 (.code) : 儲放機器可執行的指令，例如，程序的主邏輯、函數定義、控制流程
    - 執行斷 (.text) : 存儲程序的執行代碼。這個區段包含了程序的主邏輯和函數定義
    - 函數跳轉表 (.plt) : 存儲函數調用的跳轉表。它幫助實現動態連結功能
  
  - 與數據相關
    - 初始化數據段 (.data) : 存儲已初始化的全局變量和常數
    - 只讀數據段 (.rodata) : 存儲只讀的常數數據，如字符串常量、浮點數常量等
    - 未初始化數據段 (.bss) : 存儲未初始化的全局變量，在程序啟動時會自動初始化為零
    - 程序初始化段 (.init) : 存儲程序初始化器。這些初始化器在程序啟動時會被執行，以設置初始狀態
    - 程序中止段 (.fini) : 存儲程序終止器。這些終止器在程序結束時會被執行，以進行清理工作
    - .sbss (Small BSS Section) : 小型數據用
    - .sdata (Small Initialized Data Section) : 小型數據用

  - 與記憶體相關
    - 堆棧段 (.stack) : 存儲函數調用的參數、返回地址和局部變量
    - 堆段 (.heap) : 用於動態分配內存。它的大小不是固定的，可以根據需求動態擴展或縮減
    - 共享記憶體 (.comm)

  - 符號定義和管理相關
    - .globl (Global Variable Declaration)
    - .type (Type Definition)
    - .size (Size Declaration)
    - .weak (Weak Symbol Declaration)

  - 調試和跟蹤區段
    - .debug_info (Debug Information)
    - .debug_abbrev (Debug Abbreviation)
    - .debug_line (Debug Line Number)

  - 其他重要區段
    - .align (Alignment Specification)
    - .section (Section Declaration)
    - .extern (External Reference Declaration)
    - .global (Global Symbol Declaration)
    - 全局偏移表 .got (Global Offset Table) :  存儲函數和變量的地址偏移量。在動態連結時找到正確的地址

## 循址方式

- intel
  - 32bit，offset_address + (index * size) + base_address

- AT&T
  - 32bit，`base_address( offset_address, index, size )`
    > offset_address 和 index 必須是寄存器

    > size 只能是 1, 2, 4, 8，中選擇
  - 例如
    ```asm
    ; AT&T 的循址範例
    movl %edx, 4(%edi, %eax, 8)

    ; INTEL 的循址範例
    mov dword [edi + eax * 8 + 4]
    ```

## 參考
- [C 語言內聯彙編](https://dillonzq.com/2019/08/c-%E8%AF%AD%E8%A8%80%E5%86%85%E8%81%94%E6%B1%87%E7%BC%96/)

- [AT&T與Intel組合語言的比較](https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/433209/)