## 外中斷和時鐘

- 兩種中斷類型
  
  | 中斷類型 | 用途                             |
  | -------- | -------------------------------- |
  | 內中斷   | CPU內部產生的中斷，又稱軟體中斷  |
  | 外中斷   | 由外設所觸發的中斷，又稱硬體中斷 |

- 為什麼需要外中斷
  
  - CPU 透過`端口` 和 `接口` 驅動外設

    ```mermaid
    flowchart LR
    CPU --> 端口 --> 接口 --> 外設
    ```

  - 外設透過`外中斷`通知 CPU 已完成，CPU 才會繼續下一個操作

    ```mermaid
    flowchart RL
    外設 --> 觸發外中斷 --> CPU
    ```

  - CPU 運行的過程中，會自動檢查是否有外中斷需要處理
    
    ```mermaid
    flowchart LR
    獲取指令 --> 解譯指令 --> 執行指令 --> 檢查/執行中斷
    ```

  - CPU 透過 `時鐘信號(CLK)` ，驅動`指令`的運行
  
    OS 透過 `時鐘中斷`，驅使 OS 的運行，主要用於`進程的切換`

## 8086 的外中斷架構

- 8086 只有提供兩個外部中斷的接腳
  - NMI * 1，Non-Maskable-Interrpt，不可遮罩中斷
  - INTR * 1，Interrupt-Request，可遮罩中斷

## INTR 可遮罩硬體中斷

- INTR 只有一根接腳，卻需要接受多種不同設備的中斷，因此需要利用中斷管理晶片(8259)，來管理多個外設的中斷請求 (多輸入一輸出)

  ![image](hardware-interrupt/hi-1-intr-need-control-chip.png)

- 8259 可程式中斷控制晶片 (Programmable-Interrupt-Control, PIC)
  - 一個 8259 有8個中斷，若外設 >8個，可以組成`主從的架構`進行中斷的擴充，只有主要的 8259 晶片會連接到 CPU 的 INTR 接腳

    ![image](hardware-interrupt/hi-2-8259-architecture.png)
  
  - 8259 晶片上的控制端口
    
    | 端口 | 說明            | 標記              |
    | ---- | --------------- | ----------------- |
    | 0x20 | 主 PIC 命令端口 | PIC_M_CMD  (OCW2) |
    | 0x21 | 主 PIC 数据端口 | PIC_M_DATA (OCW1) |
    | 0xA0 | 從 PIC 命令端口 | PIC_S_CMD         |
    | 0xA1 | 從 PIC 数据端口 | PIC_S_DATA        |

  - 每一個 8059 的晶片上都有一個幾個控制用的寄存器
    - ICW1 - ICW4 暫存器，Initialization-Command-Word，用於初始化 8259，
    - OCW1 - OCW3 暫存器，Operation-Command-Word，用於操作 8259，
    - 8259 documentation，見 [PIC @ OS Dev](https://wiki.osdev.org/PIC)
      和 [8259 datasheet](https://pdos.csail.mit.edu/6.828/2005/readings/hardware/8259A.pdf)


- INTR 由電平觸發可遮罩硬體中斷請求，是否允許中斷由 IF 旗標控制

## NMI 不可遮罩中斷

- NMI 由上升邊緣觸發不可遮罩硬體中斷請求，不受 IF 旗標的控制，CPU在當前指令執行完畢後，必須立刻響應
  
## 以 8259 觸發 int-8 的時鐘中斷為例

- 時鐘中斷
  
  時鐘中斷是 CPU 外部的 Timer 電路產生的中斷，是一個週期性的中斷，用於更新系統時間、或 OS 觸發用於切換進程

- 8086 默認中斷向量表
 
  | 向量 | 功能            |
  | ---- | --------------- |
  | 0    | 除法溢出中斷    |
  | 1    | 单步 (用于调试) |
  | 2    | 非屏蔽中断 NMI  |
  | 3    | 断点 (用于调试) |
  | 4    | 溢出中断        |
  | 5    | 打印屏幕        |
  | 6    | 保留            |
  | 7    | 保留            |
  | 8    | 時鐘中斷        |
  | 9    | 鍵盤中斷        |
  | A    | 保留            |
  | B    | 串行通信COM2    |
  | C    | 串行通信COM1    |
  | D    | 保留            |
  | E    | 软盘控制器中斷  |
  | F    | 并行打印机中斷  |

- 8086 的中斷向量表映射到 Master-8259
  - 雖然 8259 是可以透過編程修改每一個中斷接腳對應的外設，
  
    但 8086 的設計上，將中斷向量表偏移8個位置後 (因為前八個都不是硬體中斷)，直接映射到 8259
      | 向量 | 功能                                                | 8259           |
      | ---- | --------------------------------------------------- | -------------- |
      | 0    | 除法溢出中斷                                        |                |
      | 1    | 单步 (用于调试)                                     |                |
      | 2    | 非屏蔽中断 NMI                                      |                |
      | 3    | 断点 (用于调试)                                     |                |
      | 4    | 溢出中断                                            |                |
      | 5    | 打印屏幕                                            |                |
      | 6    | 保留                                                |                |
      | 7    | 保留                                                |                |
      | 8    | 時鐘中斷                                            | IRQ-0          |
      | 9    | 鍵盤中斷                                            | IRQ-1          |
      | A    | 保留                                                | Slave-8259-out |
      | B    | 串行通信COM2                                        | IRQ-3          |
      | C    | 串行通信COM1                                        | IRQ-4          |
      | D    | AT systems: Parallel Port 2. PS/2 systems: reserved | IRQ-5          |
      | E    | 软盘控制器中斷                                      | IRQ-6          |
      | F    | 并行打印机中斷                                      | IRQ-7          |
      | 70   | CMOS Real time clock                                | Slave-IRQ-0    |
      | 71   | CGA vertical retrace                                | Slave-IRQ-1    |
      | 72   | 保留                                                | Slave-IRQ-2    |
      | 73   | 保留                                                | Slave-IRQ-3    |
      | 74   | AT systems: reserved. PS/2: auxiliary device        | Slave-IRQ-4    |
      | 75   | FPU                                                 | Slave-IRQ-5    |
      | 76   | 硬碟控制器                                          | Slave-IRQ-6    |
      | 77   | 保留                                                | Slave-IRQ-7    |
  - 示意圖

    <img src="hardware-interrupt/hi-3-interrupt-8259-map.png" width=600 height=auto>

    詳見，[intel-8259](https://en.wikipedia.org/wiki/Intel_8259)

- 流程
  * Step1，CPU 向 OCW1(PIC_M_DATA) 寫入屏蔽的控制，屏蔽其他外設的中斷，只打開時鐘中斷
    
    - 每個 8259 上有 8 bit 的 中斷，將對應的位置寫入1，代表對應的中斷被關閉

    - 例如，寫入 0x1111_1110，代表只允許 8259 上的 INT0 中斷

  * Step2，CPU 執行 STI 允許外中斷
  
  * Step3，向 OCW2(PIC_M_CMD) 寫入 0x20，表示 CPU 的中斷處理已經完成

    注意，必須通知前一個中斷已完成後，才會接受下一個中斷的請求

- 進入中斷函數後，IF 會被自動設置為 0
  以代碼中的時間中斷為例，

  進入中斷前，flag 為 0x206 = 0b 0010_0000_0110

  進入中斷後，flag 為 0x006 = 0b 0000_0000_0110，bit-9-IF: 1 -> 0

  進入中斷後，IF 會被設置為 0，代表中斷函數執行期間不允許再接受新中斷

  當中斷函數遇到 iret 返回後，才會重新將 IF 設置為 1
