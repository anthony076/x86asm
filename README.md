## x86asm Study
- 安裝 ArchLinux
  
  [install-archlinux](https://gitlab.com/anthony076/linux-usage/-/blob/main/archlinux/doc/install-arch-on-vmware.md)
  
- 建立開發環境 (bochs)

  [setup-devlop-env](doc/01_setup-develop-env-arch.md)

- 修正 bochs 的錯誤 ( 手動編譯 Bochs v2.6.11 )
  
  [bochs-fix](doc/02_bochs-fix.md)

- 開機流程
  
  [bootup-flow](doc/03_bootup-flow.md)

- x86 暫存器概述
  
  [x86-reg-overview](doc/04_x86-reg-overview.md)

- 資料類型和迴圈的使用
  
  [datatype-and-loop](doc/05_datatype%20and%20loop.md)

- 定址模式

  [address-mode](doc/06_address-mode.md)

- 算術指令和旗標暫存器

  [arithmetic-and-flags](doc/07_arithmetic_and_flags.md)

- 跳轉指令

  [jump-instructions](doc/08_jump_instructions.md)

- 堆棧和函數調用
  
  [stack-and-function-call](doc/09_stack-and-function-call.md)

- 遠調用、內中斷和異常

  [farcall-interrupt-and-error](doc/10_farcall-interrupt-and-error.md)

- 邏輯運算指令

  [input-output](doc/11_logic_operation.md)

- 輸入輸出

  [farcall-interrupt-and-error](doc/12_input_output.md)

- 字符樣式

  [character-style](doc/13_character_style.md)

- 外中斷 (以時鐘電路的外中斷為例) (import)
  
  [hardware-interrupt](doc/14_hardware_interrupt_and_clock.md)

- 讀寫硬碟
  
  [read-write-disk](doc/15_read_write_disk.md)

- 內核加載器

  [kernal-loader](doc/16_kernal-loader.md)

- 32位元架構和保護模式
  
  [32bit-and-proteched-mode](doc/17_32bit-and-proteched-mode.md)

- 檢測內存

  [detect-memory](doc/18_detect_memory.md)

- 保護模式
  
  [protected-mode](doc/19_protected-mode.md)

- 分頁與內存映射

  [page-and-memory-mapping](doc/20_memory_mapping.md)

- ELF 文件格式概述
  
  [elf-overview](doc/21_elf_overview.md)

- Linux 的 AT&T 組語格式

  [AT&T-asm-syntax](doc/22_AT&T-asm-syntax.md)

- 學習用的 gcc 配置
  
  [gcc-config-for-study](doc/22_AT&T-asm-syntax.md)

## Usage
- 建立 container
  - 方法1，透過 docker-
    - 下載 image
      
      `docker pull chinghua0731/arch-bochs`

    - 建立 container

      `docker run --name x86asm -it chinghua0731/arch-bochs bash`
    
  - 方法2，透過 docker-file
    - 建立 image
  
      `cd docker-bochs\Dockerfile-bochs27`

      `docker build --no-cache -f Dockerfile-bochs27 -t bochs27 .`

    - 建立 container

      `docker run --name bochs27 -it bochs27 bash`

- 重新進入 container
  
  `docker start bochs27`

  `docker attach bochs27`

- 在 docker-vm 中
  - 執行 bochs，`make bochs`

  - 清除檔案，`make clean`


## Ref
- nasm assembly 語法，
  
  https://www.itread01.com/content/1546226133.html

- nasm 教程
  
  http://www.bytekits.com/nasm/dollar-flag.html

- Learning Tool
  - [Self-Learning Reverse Engineering in 2022](https://www.youtube.com/watch?v=gPsYkV7-yJk)
  - [compiler-explorer](https://godbolt.org/)，線上對照c原碼`編譯`成機械碼的`比對`和`高亮`
  - [decompiler-explorer](https://dogbolt.org/)，線上對照執行檔`反編譯`成c代碼的`比對`和`高亮`
  - [decompiler-explorer使用介紹](https://binary.ninja/2022/07/13/introducing-decompiler-explorer.html)
