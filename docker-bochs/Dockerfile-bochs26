
# 目的，在 build-stage 只編譯 bochs > 複製 執行檔、lib、share > 在 release-stage 手動安裝 bochs 的依賴和其他

# step1，安裝 choco install vcxsrv
# step2，啟動 XLauch，使用預設設定，不需要修改
# step3，docker build --no-cache -f Dockerfile-bochs26 -t bochs26 .
# step4，docker run -it --rm bochs26 bash

# 問題，手動編譯造成的問題
#   在 build-stage 編譯的軟體，透過 COPY 的方式複製到 release-stage 後，
#   會因為該軟體不是透過 pacman 安裝，造成該軟體無法被 pacman 追蹤
#   在安裝其他軟體時，或安裝已經存在於 /usr/lib 的軟體，就會出現錯誤而無法安裝
#   可以透過 pacman -S 軟體名 --overwrite '*' 解決第三方軟體出現 already exist in filesystem 的問題
#   但手動編譯的軟體，仍然面臨無法被 pacman 追蹤的問題

# ================
# Build Stage
# ================
FROM archlinux:latest AS builder

WORKDIR /x86asm/src

COPY bochs-build/ /x86asm/src/
COPY bochs-build/ /x86asm/src/

# 編譯 bochs
RUN pacman --noconfirm -Sy && \
    pacman --noconfirm --disable-download-timeout -S sudo base-devel && \
    # add user
    useradd builder && \
    passwd -d builder && \
    echo "builder ALL=(ALL) ALL" >> /etc/sudoers && \
    echo "root ALL=(ALL) ALL" >> /etc/sudoers && \
    chmod 777 /x86asm/src && \
    # build bochs
    sudo -u builder makepkg -si --noconfirm

#  移除編譯環境 
RUN \
    # 將所有軟體都標記為獨立
    pacman -D --asdeps $(pacman -Qqe) && \
    # 將必要軟體都標記為明確，代表用戶明確安裝
    pacman -D --asexplicit bochs && \
    # 列出獨立軟體並移除
    pacman --noconfirm -R $(pacman -Qdtq)

# ================
# release Stage
# ================
FROM archlinux:latest AS release

WORKDIR /x86asm

# 只複製 bochs 相關
# 
COPY --from=builder /usr/bin/bximage /usr/bin/bximage 
COPY --from=builder /usr/bin/bochs /usr/bin/bochs
COPY --from=builder /usr/share/bochs /usr/share/bochs

# 缺點，占用 6xxMB，但保證可運行
#COPY --from=builder /usr/lib /usr/lib

# 優點，使 image 占用從 6xxMB 大幅降到 1.36MD
# 缺點，若只使用此行，有可能會出現 so 缺失，部分 so 放在 /usr/library，可透過 ldd /usr/bin/bochs 補上缺失的庫
COPY --from=builder /usr/lib/bochs /usr/lib/bochs

# 補上缺失的庫，可透過 ldd /usr/bin/bochs 查詢缺失的庫
COPY --from=builder /usr/lib/libltdl.so.7 /usr/lib/libltdl.so.7
COPY --from=builder /usr/lib/libSDL-1.2.so.0 /usr/lib/libSDL-1.2.so.0

# 安裝 bochs 的依賴，透過 --overwrite '*' 解決 COPY 造成 already exist 的問題
RUN pacman --noconfirm -Sy && \
    pacman --noconfirm --disable-download-timeout --overwrite '*' -S nano ttf-inconsolata && \
    pacman --noconfirm --disable-download-timeout --overwrite '*' -S atk avahi cairo desktop-file-utils fontconfig freetype2 fribidi && \
    pacman --noconfirm --disable-download-timeout --overwrite '*' -S gdbm gdk-pixbuf2 graphite gtk-update-icon-cache  gtk2 harfbuzz && \
    pacman --noconfirm --disable-download-timeout --overwrite '*' -S libcups libdaemon libdatrie libice libjpeg-turbo libpng librsvg  && \
    pacman --noconfirm --disable-download-timeout --overwrite '*' -S libxinerama libxpm libxrandr libxrender libxt core/lzo pango && \
    pacman --noconfirm --disable-download-timeout --overwrite '*' -S pixman shared-mime-info xcb-proto xorgproto hicolor-icon-theme && \
    pacman --noconfirm --disable-download-timeout --overwrite '*' -S libsm libxft libxi
    
# 從本機複製 bochs 相關檔案
COPY 2.6.11/bochsrc /x86asm/bochsrc
COPY 2.6.11/master.img /x86asm/master.img

ENV DISPLAY=host.docker.internal:0.0
