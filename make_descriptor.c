
/* 
descriptor.c 產生 descriptor 的工具
  編譯，gcc make_descriptor.c -o help
  執行，./help，會打印出結果，將結果手動貼到 loader.asm 中
  印出
    dw 0xfff
    dw 0x0
    db 0x1
    db 0x92
    db 0x40
    db 0x0
*/

#include <stdio.h>

typedef struct descriptor // 共 8個 bytes
{
    unsigned short limit_low;      // 段界限 0 ~ 15 位，short=2byte
    unsigned int base_low : 24;    // 基地址 0 ~ 23 位 16M，int=4byte
    unsigned char type : 4;        // 段类型，char=1byte
    unsigned char segment : 1;     // 1 表示代码段或数据段，0 表示系统段 ，char=1byte
    unsigned char DPL : 2;         // Descriptor Privilege Level 描述符特权等级 0 ~ 3，char=1byte
    unsigned char present : 1;     // 存在位，1 在内存中，0 在磁盘上，char=1byte
    unsigned char limit_high : 4;  // 段界限 16 ~ 19，char=1byte
    unsigned char available : 1;   // 供系統自由調用，char=1byte
    unsigned char long_mode : 1;   // 64 位扩展标志，char=1byte
    unsigned char big : 1;         // 32 位 还是 16 位，char=1byte
    unsigned char granularity : 1; // 粒度 4KB 或 1B，char=1byte
    unsigned char base_high;       // 基地址 24 ~ 31 位，char=1byte
} __attribute__((packed)) descriptor;   // 共 16byte

// __attribute__((屬性列表))，用於設定函式屬性 或 變數屬性 或 型別屬性
// __attribute__((packed))，使得變數或者結構體成員使用最小的對齊方式，即對變數是一位元組對齊，對域（field）是位對齊

void make_descriptor(descriptor *dest, unsigned int base, unsigned int limit)
{
    // printf("size of short: %x\n", sizeof(short));   // short = 2byte
    // printf("size of int: %x\n", sizeof(int));       // int   = 4byte
    // printf("size of char: %x\n", sizeof(char));     // char  = 1byte

    dest->base_low = base & 0xffffff;           // 4byte 只取低位的 3byte
    dest->base_high = (base >> 24) & 0xff;      // 4byte 只取低位的 2byte

    dest->limit_low = limit & 0xffff;           // 2byte
    dest->limit_high = (limit >> 16) & 0xf;     // 1byte

}

int main(int argc, char const *argv[])
{
    descriptor dest;
    make_descriptor(&dest, 0x10000, 0xfff);

    dest.granularity = 0; // granularity=0，粒度為1Byte
    dest.big = 1;         // 32bit的位址和操作服
    dest.long_mode = 0;   // 不是 64 位擴展
    dest.present = 1;     // 描述符在内存中
    dest.DPL = 0;         // 最高權限
    dest.segment = 1;     // 當前段描述符為數據段
    dest.type = 0b0010;   // 數據段、可寫數據、向上擴展

    // 建立指向dest值的指針，dest 是一個數組 [XX, XX, XX, XX, ...]
    char *ptr = (char *)&dest;

    // ==== 以下，ptr 指針用來移動，valueXX指針，在 ptr 的基礎上，用來取出指定長度的值 ====

    // ---- 印出 2byte 的 limit_low ----
    // value16，建立 2byte 的指針，透過 value16 取出 2byte 的值
    unsigned short value16 = *(short *)ptr;

    // 打印 bit0-15(16bit) 的 limit_low
    printf("dw 0x%x\n", value16);

    // ---- 印出 2byte 的 base_low 低16位 ----
    // 打印 bit16-39(24bit) 的 base_low，分兩次打印， 第一次16bit，第二次8bit

    // 打印完 limit_low ，移動到 base_low
    ptr += 2;   // ptr 移動 2byte

    // 根據 ptr 目前的位置，重新建立一個 2byte的指針，
    // 用來打印 base_low 的低16bit
    value16 = *(short *)ptr;
    printf("dw 0x%x\n", value16);

    // ---- 印出 1byte 的 base_low 高8位 ----
    // 打印完 base_low 低16位 ，移動到 base_low 的高8位
    ptr += 2;   

    // 根據 ptr 目前的位置，重新建立一個 1byte的指針，
    // 用來取出 1byte 的 base_low 的高8位
    unsigned char value8 = *(char *)ptr;
    printf("db 0x%x\n", value8);

    // ---- 印出 1byte 的 bit 47-40 ----
    ptr++;
    
    // 根據 ptr 目前的位置，重新建立一個 1byte的指針，
    // 用來打印 bit47-40
    value8 = *(char *)ptr;
    printf("db 0x%x\n", value8);

    // ---- 印出 1byte 的 bit 55-48 ----
    ptr++;
    value8 = *(char *)ptr;
    printf("db 0x%x\n", value8);

    // ---- 印出 1byte 的 bit 63-56 ----
    ptr++;
    value8 = *(char *)ptr;
    printf("db 0x%x\n", value8);
    
    return 0;
}
