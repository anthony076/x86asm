
.PHONY: hello.s
hello.s: src/hello.c
	gcc -m32 -fno-asynchronous-unwind-tables -fno-pic -mpreferred-stack-boundary=2 -masm=intel -S $< -o build/$@

